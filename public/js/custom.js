Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

Number.prototype.format = function(n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};


flatpickr(".datepicker", {
    minDate: (new Date()).addDays(2),
    altInput: true,
    altFormat: "j F Y",
    dateFormat: "Y-m-d",
});

flatpickr(".datetimepicker", {
    minDate: (new Date()).addDays(2),
    altInput: true,
    enableTime: true,
    altFormat: "j F Y H:i",
    dateFormat: "Y-m-d H:i:s",
});


var modal_booking_data = {};
var pilihan_mobil = [];
var mobil_terpilih = [];

$(document).on('click', '#BookingTour', function(){
    $('#Modal-Booking-Mobil').modal('show');
    loadPilihanMobil().then(function( response ){
        pilihan_mobil = response.data;
        renderJenisKendaraan();
    });
});

/**
 * Listener 
 */
$(document).on('click', '#Modal-Booking .submit', function(e){
    e.preventDefault();
    $.each($('#Modal-Booking form').serializeArray(), function() {
        modal_booking_data[this.name] = this.value;
    });

    $('#Modal-Booking').modal('hide');

    if ( $(this).hasClass('booking-mobil') ) {
        // BOOKING MOBIL
        // Check Car Availability
        loadPilihanMobil( {id: modal_booking_data.jenis_kendaraan_id, available: modal_booking_data.tanggal_pickup})
            .then(function( response ){
                console.log( response );
                if( response.data.length > 0 ) {
                    // Car is Available Go to Next Step
                    mobil_terpilih = response.data[0];
                    $('#Modal-Booking-Konfirmasi').modal('show');
                    $('#Modal-Booking-Konfirmasi .modal-body').html( getRenderedBookingInfo() );
                } else {
                    alert('Mobil pada tanggal tersebut sudah dipesan');
                }
            });
    } else {
        // BOOKING PAKET WISATA
        modal_booking_data['jenis_kendaraan_id'] = null;
        $('#Modal-Booking').modal('hide');
        $('#Modal-Booking-Konfirmasi .modal-body').html( getRenderedBookingInfo() );
        $('#Modal-Booking-Konfirmasi').modal('show');
    }
});

/**
 * Listener Pilihan Mobil on Change
 */
$(document).on('click', '#Modal-Booking-Mobil .pilihan-mobil', function(e){
    e.preventDefault();
    var jenis_kendaraan_id = $(this).data('jenisKendaraanId');
    modal_booking_data['jenis_kendaraan_id'] = jenis_kendaraan_id;

    mobil_terpilih =_.find( pilihan_mobil, {id: jenis_kendaraan_id});

    renderJenisKendaraan();
});

$(document).on('click', '#Modal-Booking-Mobil .back', function(e){
    e.preventDefault();
    $('#Modal-Booking-Mobil').modal('hide');
    $('#Modal-Booking').modal('show');
});

$(document).on('click', '#Modal-Booking-Konfirmasi .back', function(e){
    e.preventDefault();
    $('#Modal-Booking-Konfirmasi').modal('hide');
    $('#Modal-Booking').modal('show');
});

/**
 * SUBMIT TO CONFIRMATION
 */
$(document).on('click', '#Modal-Booking-Mobil .submit', function(e){
    e.preventDefault();


    var $modalBooking = $('#Modal-Booking');
    $modalBooking.find('.label-penumpang').text('Jumlah Penumpang');

    if ( modal_booking_data['jenis_kendaraan_id'] === null ) {
        alert('Pilih mobil terlebih dahulu');
        return false;
    }

    if ( modal_booking_data['jenis_kendaraan_id'] == 0 ) {
        var confirmation = confirm('Anda memesan paket tour dan akan menyediakan mobil sendiri. Apakah anda yakin?');

        if ( ! confirmation ) return false;

        $modalBooking.find('.label-penumpang').text('Jumlah Peserta Tour');
    }    

    /*$('#Modal-Booking').modal('hide');*/
    $('#Modal-Booking-Mobil').modal('hide');
    $('#Modal-Booking').modal('show');
    if( ! mobil_terpilih ) {
        $('#Modal-Booking').find('.alamat-pickup').hide();
        $('#Modal-Booking').find('.alamat-pickup input').val('');
    } else {
        $('#Modal-Booking').find('.alamat-pickup').show();
    }

    $('#Modal-Booking-Konfirmasi .modal-body').html( getRenderedBookingInfo() );
});

/**
 * SUBMIT RESERVATION TO SERVER
 */
$(document).on('click', '#Modal-Booking-Konfirmasi .submit', function(e){
    e.preventDefault();
    if( mobil_terpilih ) {
        modal_booking_data.jenis_kendaraan_id = mobil_terpilih.id;
    }
    $.ajax({
        url: window.APP_URL + '/reservasi',
        type: 'post',
        data: modal_booking_data,
        success: function( response ){
           window.location.href = window.APP_URL + '/reservasi/' + response.kode_reservasi;
        },
        error: function( error ){
            console.log( error )
            alert( error.responseJSON.message );
        }
    });
});

/**
 * Load Pilihan Mobil
 */
function loadPilihanMobil( data ){
    return $.ajax({
        url: window.APP_URL + '/jenis-kendaraan',
        dataType: 'json',
        data: data
    });
}

/**
 * Render Jenis Kendaraan to Modal
 */
function renderJenisKendaraan(){
    $('#Modal-Booking-Mobil .list-kendaraan').html( getRenderedJenisKendaraan() );
}

/**
 * Get Rendered Jenis Kendaraan
 */
function getRenderedJenisKendaraan(){
    var html = '';
    
    if( pilihan_mobil.length > 0 ) {
        $.each(pilihan_mobil, function(i, jenis_kendaraan){
            html = html + `
                <div class="pilihan-mobil media mb-3 align-items-center border-bottom pb-3 ${modal_booking_data.jenis_kendaraan_id == jenis_kendaraan.id ? 'active' : ''}" data-jenis-kendaraan-id="${jenis_kendaraan.id}">
                    <img src="${window.APP_URL}/storage/${jenis_kendaraan.image}" class="mr-2" width="75">
                    <div class="media-body">
                        <h5 class="mb-0">${jenis_kendaraan.name}</h5>
                        <p class="mb-0">${jenis_kendaraan.price}</p>
                        <span>${jenis_kendaraan.seat} Seat</span>
                    </div>
                    <div class="custom-control custom-radio">
                        <input type="radio" ${modal_booking_data.jenis_kendaraan_id == jenis_kendaraan.id ? 'checked' : ''} name="jenis_kendaraan_id" class="custom-control-input">
                        <label class="custom-control-label"></label>
                    </div>
                </div>
            `;
        });
    } else {
        html = html + `
                <div class="alert alert-danger">
                    Semua kendaraan telah dipesan pada tanggal tersebut.
                </div>
            `;
    }

    html = html + `
    <div class="pilihan-mobil media mb-3 align-items-center border-bottom pb-3 ${modal_booking_data.jenis_kendaraan_id == 0 ? 'active' : ''}" data-jenis-kendaraan-id="0">
        <img src="${window.APP_URL}/img/no-car.png" class="mr-2" width="75">
        <div class="media-body">
            <h5 class="mb-0">Mobil Sendiri</h5>
            <p class="mb-0">Gunakan mobil yang anda sediakan.</p>
        </div>
        <div class="custom-control custom-radio">
            <input type="radio" ${modal_booking_data.jenis_kendaraan_id == 0? 'checked' : ''} name="jenis_kendaraan_id" class="custom-control-input">
            <label class="custom-control-label"></label>
        </div>
    </div>`;

    return html;
}

/**
 * Get Rendered Booking Info
 */
function getRenderedBookingInfo(){
    var html = '';

    html = html + `
        <div class="form-group">
            <small class="text-muted">Paket Tour</small>
            <p>${modal_booking_data.paket_tour ? modal_booking_data.paket_tour : '-'}</p>
        </div>
        <div class="form-group">
            <small class="text-muted">Nama</small>
            <p>${modal_booking_data.name}</p>
        </div>
        <div class="form-group">
            <small class="text-muted">Email</small>
            <p>${modal_booking_data.email}</p>
        </div>
        <div class="form-group">
            <small class="text-muted">Nomor HP</small>
            <p>${modal_booking_data.nomor_hp}</p>
        </div>
        <div class="form-group">
            <small class="text-muted">Jumlah Penumpang</small>
            <p>${modal_booking_data.jumlah_penumpang ? modal_booking_data.jumlah_penumpang + ' Orang' : 'Sesuai Kapasitas'}</p>
        </div>
        <div class="form-group">
            <small class="text-muted">Tanggal Tour</small>
            <p>${modal_booking_data.tanggal_pickup}</p>
        </div>
    `;

    if( mobil_terpilih ) {
        html = html + `
            <div class="form-group alamat-pickup">
                <small class="text-muted">Alamat Pickup</small>
                <p>${modal_booking_data.alamat_pickup}</p>
            </div>
            <div class="form-group">
                <small class="text-muted">Mobil</small>
                <p>${mobil_terpilih.name}</p>
            </div>`;
    }

    return html;
}

$(document).on('change', '.form-control.listen', function(e){

    var data = {};

    data[ $(this).attr('name') ] = $(this).val();

    $.ajax({
        url: $(this).data('url'),
        type: 'PATCH',
        data: data,
        success: function(){
            window.location.reload();
        }
    });
});


var table_index = 10;
$(document).on('click', '#Tabel-Harga-Wisata .action-delete', function(e){
    e.preventDefault();
    $(this).parent().parent().remove();
});
$(document).on('click', '#Tabel-Harga-Wisata .action-add-price', function(e){
    e.preventDefault();
    table_index = table_index + 1;
    $('#Tabel-Harga-Wisata table tbody').append( getNewHargaWisataTable() );
});

function getNewHargaWisataTable (){
    return `
    <tr>
        <th>
            <button class="btn btn-sm btn-danger action-delete"><i class="fa fa-trash"></i></button>
        </th>
        <th>
            <input type="number" min="1" required class="form-control" value="1" name="harga_paket_wisata[${table_index}][jumlah_penumpang]">
        </th>
        <th>
            <input type="number" min="1" required class="form-control" value="" name="harga_paket_wisata[${table_index}][price]">
        </th>
    </tr>
    `;
}

$(document).on('change', '#Form-Reservasi [name="jumlah_penumpang"]', function(e){
    var PaketWisata = $('#Form-Reservasi [name="paket_tour_id"]');
    setHargaPaketWisata( PaketWisata.val(), $(this).val() );
});
function setHargaPaketWisata( paket_tour_id, jumlah_penumpang ) {
    $.ajax({
        url: window.APP_URL + '/paket-tour/' + paket_tour_id + '/harga',
        data: {
            action: 'calculate',
            jumlah_penumpang: jumlah_penumpang
        }, 
        success: function( response ){
            $('#Form-Reservasi [name="harga_paket_wisata"]').val( response.price );
            calculateTotalPriceReservasi();
        }
    });
}

$(document).on('change', '#Form-Reservasi [name="jenis_kendaraan_id"]', function(e){
    setHargaKendaraan( $(this).val() );
    updateKendaraanList( $(this).val() );
});
function setHargaKendaraan( jenis_kendaraan_id){
    loadPilihanMobil({
        id: jenis_kendaraan_id
    }).then(function( response ){
        var jenis_kendaraan = response.data[0];

        $('#Form-Reservasi [name="harga_jenis_kendaraan"]').val( jenis_kendaraan.price );
        calculateTotalPriceReservasi();
    })
}


function updateKendaraanList( jenis_kendaraan_id ) {
    var target_form = $('#Form-Reservasi [name="kendaraan_id"]');
    target_form.prop('disabled', true);

    loadPilihanMobil({
        id: jenis_kendaraan_id
    }).then(function( response ){
        target_form.val(null);
        target_form.html('<option>Pilih Kendaraan</option>');

        if( response.data[0].kendaraan.length > 0 ) {
            console.log( response );
            $.each(response.data[0].kendaraan, function( i, kendaraan ){
                target_form.append('<option>'+ kendaraan.nomor_polisi + ' - ' + kendaraan.warna +'</option>');
            });
        }
        target_form.prop('disabled', false);
    });
}

$(document).on('change', '#Form-Reservasi [name="harga_paket_wisata"], #Form-Reservasi [name="harga_jenis_kendaraan"]', function(e){
    calculateTotalPriceReservasi();
});

function calculateTotalPriceReservasi(){
    var harga_1 = $('#Form-Reservasi [name="harga_paket_wisata"]').val();
    var harga_2 = $('#Form-Reservasi [name="harga_jenis_kendaraan"]').val();

    $('#Form-Reservasi [name="total"]').val( parseInt( harga_1) + parseInt( harga_2 ) );
}



/** SCROLLING */
function check_header() {
    if ( ! $('.main-header').hasClass('autosticky') ) {
            return;
    }
    var scroll = $(window).scrollTop();

    if ( $(window).width() >= 991 ) {
            if( ! $('.main-header').hasClass('fixed-top') ) {
                    $('.main-header').addClass('fixed-top');
            }

        if ( scroll > 1 ) {
            $('.main-header').removeClass('navbar-dark');
            $('.main-header').addClass('bg-white scrolled navbar-light');
        } else {
            $('.main-header').removeClass('bg-white navbar-light scrolled');
            $('.main-header').addClass('navbar-dark');
        }
    } else {
            $('.main-header').removeClass('navbar-dark scrolled');
        $('.main-header').addClass('bg-white navbar-light');
            $('.main-header').removeClass('fixed-top');
    }
}


$(document).ready(function (event) {
        check_header();
});
$(window).resize(function (event) {
        check_header();
});

$(window).scroll(function (event) {
    check_header();
});

$(document).on('submit', '.form-confirm-delete', function( e ){
    var confirmation = confirm('Hapus data ini?');

    if ( ! confirmation ) {
        e.preventDefault();
    }
});


var formKonfirmasiPembayaran = $('#FormKonfirmasiPembayaran');
if ( formKonfirmasiPembayaran.length > 0 ) {
    var reservasi = formKonfirmasiPembayaran.data('reservasi');
    formKonfirmasiPembayaran.find('select[name="reservasi_id"]').on('change', function(e){
        if( $(this).val() ) {
            formKonfirmasiPembayaran.find('#detail-konfirmasi-pembayaran').show();
        } else {
            formKonfirmasiPembayaran.find('#detail-konfirmasi-pembayaran').hide();
        }
    });

    formKonfirmasiPembayaran.find('select[name="jenis_pembayaran"]').on('change', function(e){
        var value = $(this).val();
        var $fieldJumlahTransfer = formKonfirmasiPembayaran.find('#field-jumlah-transfer');
        if( value ) {
            $fieldJumlahTransfer.show();
            var $input = $fieldJumlahTransfer.find('input')
            if( value == 'full' ) {
                
                $input.prop('readonly', true);
                reservasi.forEach(function(item){
                    if( item.id == formKonfirmasiPembayaran.find('select[name="reservasi_id"]').val() ) {
                        $input.val( item.total );
                    }
                });         
            } else {
                $input.val('').prop('readonly', false);

            }
        } else {
            formKonfirmasiPembayaran.find('#field-jumlah-transfer').hide();
        }
    });
}