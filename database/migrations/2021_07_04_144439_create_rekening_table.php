<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRekeningTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rekening', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('account_number');
            $table->string('account_name');
        });

        Schema::table('konfirmasi_pembayarans', function( $table ){
            $table->dropColumn('rekening_tujuan');
            $table->integer('rekening_id')->unsigned();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rekening');

        Schema::table('konfirmasi_pembayarans', function( $table ){
            $table->dropColumn('rekening_id');
            $table->string('rekening_tujuan');
        });
    }
}
