<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSepedaMotorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sepeda_motor', function (Blueprint $table) {
            $table->string('kode_motor')->primary();
            $table->string('nama_motor')->nullable();
            $table->string('jenis_motor')->nullable();
            $table->string('merk_motor')->nullable();
            $table->integer('cc')->nullable();
            $table->string('warna')->nullable();
            $table->integer('harga')->nullable();
            $table->date('tanggal_launching')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sepeda_motor');
    }
}
