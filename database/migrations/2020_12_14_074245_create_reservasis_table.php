<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservasis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('paket_tour_id')->nullable();
            $table->integer('jenis_kendaraan_id')->nullable();
            $table->integer('kendaraan_id')->nullable();
            $table->integer('driver_id')->nullable();
            $table->integer('status_reservasi_id')->default(1);

            $table->integer('harga_paket_tour')->nullable();
            $table->integer('harga_jenis_kendaraan')->nullable();
            $table->integer('total')->nullable();
            
            $table->string('kode_reservasi')->nullable()->unique();
            $table->string('name');
            $table->string('email');
            $table->string('nomor_hp')->nullable();
            $table->integer('jumlah_penumpang')->nullable();
            $table->string('alamat_pickup')->nullable();
            $table->date('tanggal_pickup')->nullable();
            $table->date('estimasi_selesai')->nullable();
            $table->string('status')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservasis');
    }
}
