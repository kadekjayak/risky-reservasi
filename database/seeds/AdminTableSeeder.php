<?php

use Illuminate\Database\Seeder;
use App\Admin;
use Illuminate\Support\Facades\Storage;
use App\User;
class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
           'name' => 'admin',
           'email' => 'admin@test.net',
           'role'   => 'administrator',
           'password' => bcrypt('123456'),
           'nomor_hp' => '123456',
        ]);

        Admin::create([
            'name' => 'admin',
            'email' => 'owner@test.net',
            'role'   => 'owner',
            'password' => bcrypt('123456'),
            'nomor_hp' => '123456',
         ]);


         User::create([
            'name' => 'User',
            'email' => 'user@test.net',
            'password' => bcrypt('123456'),
            'nomor_hp' => '123456',
         ]);
    }
}
