<?php

use Illuminate\Database\Seeder;
use App\Models\JenisKendaraan;
use Illuminate\Support\Facades\Storage;

class JenisKendaraanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jenis_kendaraan = JenisKendaraan::create([
            'name' => 'Toyota Avanza',
            'seat' => '6',
            'description' => '<p>Mobil sejuta umat</p>',
            'price' => 300000,
            'image' => 'jenis-kendaraan/avanza.jpg'
        ]);
        

        $jenis_kendaraan->kendaraan()->create([
            'nomor_polisi' => 'DK 123',
            'warna' => 'Hitam',
            'tahun' => 2020,

        ]);

        $jenis_kendaraan = JenisKendaraan::create([
            'name' => 'Toyota Alphard',
            'seat' => '2',
            'price' => 1300000,
            'image' => 'jenis-kendaraan/alphard.jpg'
        ]);

        $jenis_kendaraan->kendaraan()->create([
            'nomor_polisi' => 'DK 123',
            'warna' => 'Hitam',
            'tahun' => 2020
        ]);

        $jenis_kendaraan = JenisKendaraan::create([
            'name' => 'Formula 1',
            'seat' => '1',
            'price' => 11300000,
            'image' => 'jenis-kendaraan/f1.jpg'
        ]);

        $jenis_kendaraan = JenisKendaraan::create([
            'name' => 'Honda Brio',
            'seat' => '4',
            'price' => 200000,
            'image' => 'jenis-kendaraan/brio.jpg'
        ]);

        $jenis_kendaraan->kendaraan()->create([
            'nomor_polisi' => 'DK 123',
            'warna' => 'Hitam',
            'tahun' => 2020
        ]);
    }
}
