<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(StatusReservasiTableSeeder::class);
        $this->call(DriverTableSeeder::class);
        $this->call(JenisKendaraanTableSeeder::class);
        $this->call(PaketWisataTableSeeder::class);
        $this->call(AdminTableSeeder::class);
    }
}
