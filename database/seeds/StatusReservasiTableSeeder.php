<?php

use Illuminate\Database\Seeder;
use App\Models\StatusReservasi;
use Illuminate\Support\Facades\Storage;

class StatusReservasiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status_reservasi = [
            ['name' => 'Menunggu Pembayaran'],
            ['name' => 'Down Payment'],
            ['name' => 'Dibayar'],
            ['name' => 'Dalam Perjalanan'],
            ['name' => 'Selesai'],
        ];

        foreach( $status_reservasi as $status ) {
            StatusReservasi::create( $status );
        }
    }
}
