<?php

use Illuminate\Database\Seeder;
use App\Models\Driver;
use Illuminate\Support\Facades\Storage;

class DriverTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Driver::create([
           'name' => 'Rally Driver',
           'alamat' => 'Mud Road',
           'nomor_hp' => '080989999',
           'jenis_kelamin' => 1
        ]);
        Driver::create([
            'name' => 'Valentino Rossy',
            'alamat' => 'Mud Road',
            'nomor_hp' => '080989999',
            'jenis_kelamin' => 1
         ]);
    }
}
