<?php

namespace App;

class Helper {
    public static function formatMoney( $number )
    {
        return number_format( $number, 0, ',', '.' );
    }
}