<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PaketTour as Model;
use Illuminate\Support\Facades\Storage;

class PaketTourController extends Controller
{
    /**
     * Index
     */
    public function index()
    {
        $items = Model::paginate(15);
        return view('paket-tour.index', compact('items') );
    }

    public function show( $id )
    {
        $model = Model::findOrFail( $id );
        return view('paket-tour.show', compact('model') );
    }
}
