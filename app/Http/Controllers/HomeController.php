<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PaketTour;
use App\Models\JenisKendaraan;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paket_wisata = PaketTour::limit(3)->get();

        $jenis_kendaraan = JenisKendaraan::limit(4)->get();
        return view('home', compact('paket_wisata', 'jenis_kendaraan'));
    }
}
