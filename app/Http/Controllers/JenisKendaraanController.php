<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\JenisKendaraan as Model;
use App\Models\Reservasi;
use Illuminate\Support\Facades\Storage;

class JenisKendaraanController extends Controller
{
    /**
     * Index
     */
    public function index( Request $request )
    {
        $query = Model::query()->with(['kendaraan']);

        if( $id = $request->input('id') ) {
            $query->where('id', $id);
        }


        if ( $min_seat = $request->input('min_seat') ) {
            $query->where('seat', '>=', $min_seat);
            $query->orderBy('seat', 'ASC');
        }

        if ( $available = $request->input('available') ) {
            $query->whereHas('kendaraan', function( $q ) use( $available ){
                return $q->whereNotIn('id', Reservasi::select('kendaraan_id')->whereNotNull('kendaraan_id')->where('tanggal_pickup', $available)->pluck('kendaraan_id')->toArray() );
            });

            $query->withCount('kendaraan');
        }

        if ( $request->ajax() ) {
            return response()->json( $query->paginate(15) );
        }

        $items = $query->paginate();

        return view('jenis-kendaraan.index', compact('items') );
    }

    public function show( $id )
    {
        $model = Model::findOrFail( $id );
        return view('jenis-kendaraan.show', compact('model') );
    }
}
