<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\KonfirmasiPembayaran as Model;
use Illuminate\Support\Facades\Storage;
use App\Models\Reservasi;
use App\Http\Requests\StoreKonfirmasiPembayaran;
use App\Models\Rekening;

class KonfirmasiPembayaranController extends Controller
{

    public function index()
    {
        //DB & Menunggu Pembayaran
        $rekening = Rekening::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();



        $reservasi = Reservasi::whereIn('status_reservasi_id', [1,2])->get();
        return view('konfirmasi-pembayaran.index', compact('reservasi', 'rekening'));
    }

    public function store( StoreKonfirmasiPembayaran $request )
    {
        $model = new Model( $request->input() );
        if ( $image = $request->file('image') ) {
            $path = $image->store('konfirmasi-pembayaran');
            $model->image = $path;
        }

        $model->save();

        flash('Konfirmasi pembayaran berhasil disimpan. Kami akan segera memverifikasi pembayaran anda.')->success();

        return redirect()->back()->withInput();
    }
}

