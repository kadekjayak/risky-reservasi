<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Reservasi as Model;
use App\Models\Kendaraan;
use App\Models\PaketTour;
use App\Models\JenisKendaraan;
use Illuminate\Support\Facades\Storage;
use App\Mail\MenungguPembayaranEmail;
use \Mail;

class ReservasiController extends Controller
{

    /**
     * Index
     */
    public function index( Request $request )
    {
        $query = Model::query()->with(['paketTour', 'jenisKendaraan', 'kendaraan', 'driver', 'statusReservasi']);

        $items = $query->where('user_id', $request->user()->id )->paginate(15);

        return view('akun.reservasi.index', compact( 'items' ) );
    }

    /**
     * Store
     */
    public function store( Request $request )
    {
        $model = new Model( $request->input() );
        $model->kode_reservasi = date('y') . ( date('z') + 1 ) . ( Model::max('id') + 1 ) ;

        if ( $paket_wisata_id = $request->input('paket_tour_id') ) {
            $paket_wisata = PaketTour::findOrFail( $paket_wisata_id );
            $model->harga_paket_tour = $paket_wisata->getPrice( $model->jumlah_penumpang );
        }

        if ( $jenis_kendaraan_id = $request->input('jenis_kendaraan_id') ) {
            $jenis_kendaraan = JenisKendaraan::find( $jenis_kendaraan_id );
            $available_kendaraan = Kendaraan::where('jenis_kendaraan_id', $jenis_kendaraan->id)->availableAt( $model->tanggal_pickup )->first();

            if( ! $available_kendaraan ) {
                abort(422, 'Kendaraan tidak tersedia pada tanggal tersebut');
            }
            
            $model->kendaraan_id = $available_kendaraan->id;
            $model->harga_jenis_kendaraan = $jenis_kendaraan->price;
        }

        $model->user_id = $request->user() ? $request->user()->id : null;
        $model->total = (int)$model->harga_paket_wisata + (int)$model->harga_jenis_kendaraan;
        $model->save();
        
        Mail::to( $request->user()->email )->send( new MenungguPembayaranEmail( $model ) );

        return response()->json( $model );
    }

    public function show( $kode_reservasi, Request $request )
    {
        $model = Model::where('kode_reservasi', $kode_reservasi)->with(['paketTour', 'jenisKendaraan', 'driver'])->firstOrFail();

        if ( $request->ajax() ) {
            return response()->json( $model );
        }

        return view('reservasi.show', compact( 'model' ) );
    }
}

