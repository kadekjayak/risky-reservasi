<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class VerifyEmailController extends Controller
{
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index(Request $request)
    {
        $token = $request->query('token');
        $user = User::where('verification_token', $token)->firstOrFail();

        $user->email_verified_at = \Carbon\Carbon::now();
        $user->save();

        Auth::login( $user );

        return redirect()->intended( $this->redirectTo );
    }
}
