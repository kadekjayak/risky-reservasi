<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{

    /**
     * Index
     */
    public function index( Request $request )
    {
        $user = $request->user();
        return view('akun.index', compact( 'user' ) );
    }

    /**
     * Store
     */
    public function store( Request $request )
    {
        $model = $request->user();
        $model->fill( $request->input() );
        $model->save();

        return redirect()->back();
    }


}

