<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PaketTour as Model;
use Illuminate\Support\Facades\Storage;

class PaketTourHargaController extends Controller
{
    /**
     * Index
     */
    public function index( $paket_wisata_id, Request $request )
    {
        $model = Model::findOrFail( $paket_wisata_id );
        
        if ( $action = $request->input('action') ) {
            if( $action == 'calculate' ) {
                $harga = $model->hargaPaketTour()->where('jumlah_penumpang', '>=', $request->input('jumlah_penumpang'))->orderBy('jumlah_penumpang', 'ASC')->first();

                if( ! $harga ) {
                    abort( 422, 'Harga tidak ditemukan');
                }

                return response()->json( $harga );
            }
        }

    }
}
