<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Artisan;

class MigrateController extends Controller
{
    /**
     * Index
     */
    public function index()
    {
        Artisan::call('migrate:fresh');
        Artisan::call('db:seed');
        echo 'OK';
    }

}
