<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin as Model;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    /**
     * Index
     */
    public function index()
    {
        $items = Model::paginate(15);
        return view('admin.admin.index', compact('items') );
    }

    /**
     * Create
     */
    public function create()
    {
        $model = new Model();
        return view('admin.admin.create', compact('model') ); 
    }

    /**
     * Store
     */
    public function store( Request $request )
    {
        $model = new Model( $request->except('password') );

        $model->password = bcrypt( $request->input('password') );
        $model->save();

        flash('Data Berhasil disimpan')->success();
        return redirect()->route('admin.admin.edit', $model->id);
    }

    /**
     * Edit
     */
    public function edit( $model_id, Request $request )
    {
        $model = Model::findOrFail( $model_id );
        return view('admin.admin.edit', compact('model') );
    }

    /**
     * Update
     */
    public function update( $model_id, Request $request )
    {
        $model = Model::findOrFail( $model_id );

        $model->fill( $request->except('password') );

        if( $password = $request->input('password') ) {
            $model->password = bcrypt( $password );
        }

        $model->save();

        flash('Data Berhasil disimpan')->success();

        return redirect()->back()->withInput();
    }

    /**
     * Destroy
     */
    public function destroy( $model_id, Request $request )
    {   
        $model = Model::findOrFail( $model_id );

        if( $request->user()->id == $model->id ) {
            flash('Tidak dapat menghapus akun sendiri!')->error();
            return redirect()->back();
        }
        $model->delete();
        flash('Data Berhasil Dihapus')->success();
        return redirect()->back();
    }
}
