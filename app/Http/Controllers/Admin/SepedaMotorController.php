<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SepedaMotor as Model;

class SepedaMotorController extends Controller
{
    /**
     * Index
     */
    public function index()
    {
        $items = Model::paginate(15);
        return view('admin.sepeda-motor.index', compact('items') );
    }

    /**
     * Create
     */
    public function create()
    {
        $jenis_motors = ['sport' => 'Sport', 'matic' => 'Matic', 'offroad' => 'Offroad'];
        $merk_motors = [ 'yamaha' => 'Yamaha', 'honda' => 'Honda', 'dll' => 'Lainnya'];
        $model = new Model();
        return view('admin.sepeda-motor.create', compact('model', 'jenis_motors', 'merk_motors') ); 
    }

    /**
     * Store
     */
    public function store( Request $request )
    {
        $paket_wisata = Model::create( $request->input() );
        flash('Data Berhasil disimpan')->success();
        return redirect()->back()->withInput();
    }

    /**
     * Edit
     */
    public function edit( $id, Request $request )
    {
        $jenis_motors = ['sport' => 'Sport', 'matic' => 'Matic', 'offroad' => 'Offroad'];
        $merk_motors = [ 'yamaha' => 'Yamaha', 'honda' => 'Honda', 'dll' => 'Lainnya'];
        $model = Model::where('kode_motor',  $id)->firstOrFail();
        return view('admin.sepeda-motor.edit', compact('model', 'jenis_motors', 'merk_motors') ); 
    }

    /**
     * Update
     */
    public function update( $id, Request $request )
    {
        
        $model = Model::where('kode_motor', $id)->firstOrFail();
        
        $model->fill( $request->input() );
        
        $model->save();

        flash('Data Berhasil disimpan')->success();

        return redirect()->route('admin.sepeda-motor.index');
    }

    /**
     * Destroy
     */
    public function destroy( $id, Request $request )
    {   
        $model = Model::where('kode_motor', $id)->firstOrFail();
        $model->delete();
        flash('Data Berhasil Dihapus')->success();
        return redirect()->route('admin.sepeda-motor.index');
    }
}
