<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Reservasi;

class DashboardController extends Controller
{
    /**
     * Index
     */
    public function index()
    {
        $today_schedule = Reservasi::with(['paketTour', 'kendaraan.jenisKendaraan', 'driver'])->whereDate('tanggal_pickup', \Carbon\Carbon::now())->get();
        $next_schedule = Reservasi::orderBy('tanggal_pickup', 'ASC')->with(['paketTour', 'jenisKendaraan', 'kendaraan', 'driver'])->whereDate('tanggal_pickup', '>', \Carbon\Carbon::now())->get();

        
        return view('admin.dashboard', compact('today_schedule', 'next_schedule' ));
    }

}
