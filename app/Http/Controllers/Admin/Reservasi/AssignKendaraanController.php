<?php

namespace App\Http\Controllers\Admin\Reservasi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Reservasi as Model;

class AssignKendaraanController extends Controller
{
    /**
     * Update
     */
    public function update( $model_id, Request $request )
    {
        $model = Model::findOrFail( $model_id );

        $model->fill( $request->only(['kendaraan_id']) );
        $model->save();

        // LOG ASSIGN KENDARAAN

        flash('Data Berhasil disimpan')->success();

        return redirect()->back()->withInput();
    }
}
