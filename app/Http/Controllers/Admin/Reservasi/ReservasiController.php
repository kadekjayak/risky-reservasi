<?php

namespace App\Http\Controllers\Admin\Reservasi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Reservasi as Model;
use App\Models\StatusReservasi;
use App\Models\Driver;
use App\Models\PaketTour;
use App\Models\JenisKendaraan;
use App\Models\Kendaraan;
use Mail;
use App\Mail\ReservasiStatusEmail;

class ReservasiController extends Controller
{
    /**
     * Index
     */
    public function index( Request $request )
    {
        $items = Model::with(['statusReservasi']);
        $status_reservasi = StatusReservasi::pluck('name', 'id');

        if( $search = $request->input('search') ) {
            $items->where('name', 'LIKE', "%". $search ."%");
        }

        if( $status_reservasi_id = $request->input('status_reservasi_id') ) {
            $items->where('status_reservasi_id', $status_reservasi_id);
        }

        $items = $items->paginate(15);
        return view('admin.reservasi.index', compact('items', 'status_reservasi') );
    }

    /**
     * Show
     */
    public function show( $id )
    {
        $drivers = Driver::pluck('name', 'id');
        $status_reservasi = StatusReservasi::pluck('name', 'id');
        $model = Model::with(['paketTour', 'jenisKendaraan', 'kendaraan', 'driver', 'konfirmasiPembayaran'])->findOrFail( $id );
        return view('admin.reservasi.show', compact('model', 'status_reservasi', 'drivers') );
    }

    /**
     * Create
     */
    public function create()
    {
        $paket_wisatas = PaketTour::pluck('name', 'id');
        $jenis_kendaraans = JenisKendaraan::all();
        $kendaraans = Kendaraan::all();
        $status_reservasi = StatusReservasi::pluck('name', 'id');
        $drivers = Driver::pluck('name', 'id');
        $model = new Model();
        return view('admin.reservasi.create', compact('model', 'paket_wisatas', 'jenis_kendaraans', 'status_reservasi', 'drivers', 'kendaraans') ); 
    }

    /**
     * Store
     */
    public function store( Request $request )
    {
        $paket_wisata = new Model( $request->input() );
        $paket_wisata->save();
        
        flash('Data Berhasil disimpan')->success();
        return redirect()->back()->withInput();
    }

    /**
     * Edit
     */
    public function edit( $id_reservasi, Request $request )
    {
        $model = Model::findOrFail( $id_reservasi );
        $drivers = Driver::pluck('name', 'id');
        $paket_wisatas = PaketTour::pluck('name', 'id');
        $jenis_kendaraans = JenisKendaraan::all();
        $kendaraans = collect([]);

        if ( $model->jenis_kendaraan_id ) {
            $kendaraans = $model->jenisKendaraan->kendaraan;
        } 
        
        $status_reservasi = StatusReservasi::pluck('name', 'id');
        return view('admin.reservasi.edit', compact('model', 'drivers', 'paket_wisatas', 'jenis_kendaraans', 'status_reservasi', 'kendaraans') );
    }

    /**
     * Update
     */
    public function update( $id_paket_wisata, Request $request )
    {
        
        $model = Model::findOrFail( $id_paket_wisata );
        
        if ( $request->input('status_reservasi_id') != $model->status_reservasi_id ) {
            Mail::to( $model->user )->send( new ReservasiStatusEmail($model));
        }

        $model->fill( $request->input() );
        $model->save();
 
        flash('Data Berhasil disimpan')->success();

        if ( $request->ajax() ) {
            return response()->json( $model );
        }

        return redirect()->back()->withInput();
    }

    /**
     * Destroy
     */
    public function destroy( $id_paket_wisata )
    {   
        $model = Model::findOrFail( $id_paket_wisata );
        $model->delete();
        flash('Data Berhasil Dihapus')->success();
        return redirect()->back();
    }
}
