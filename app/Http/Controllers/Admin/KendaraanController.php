<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\JenisKendaraan;
use App\Models\Kendaraan as Model;

class KendaraanController extends Controller
{
    /**
     * Index
     */
    public function index()
    {
        $items = Model::paginate(15);
        return view('admin.kendaraan.index', compact('items') );
    }

    /**
     * Create
     */
    public function create()
    {
        $jenis_kendaraan = JenisKendaraan::pluck('name', 'id');
        $model = new Model();
        return view('admin.kendaraan.create', compact('model', 'jenis_kendaraan') ); 
    }

    /**
     * Store
     */
    public function store( Request $request )
    {
        if ( $request->user()->role != 'administrator' ) {
            flash('Hanya admin yang dapat melakukan perubahan.')->error();
            return redirect()->back();
        }

        $paket_wisata = Model::create( $request->input() );
        flash('Data Berhasil disimpan')->success();
        return redirect()->back()->withInput();
    }

    /**
     * Edit
     */
    public function edit( $id_paket_wisata, Request $request )
    {
        $jenis_kendaraan = JenisKendaraan::pluck('name', 'id');
        $model = Model::findOrFail( $id_paket_wisata );
        return view('admin.kendaraan.edit', compact('model', 'jenis_kendaraan') );
    }

    /**
     * Update
     */
    public function update( $id_paket_wisata, Request $request )
    {
        if ( $request->user()->role != 'administrator' ) {
            flash('Hanya admin yang dapat melakukan perubahan.')->error();
            return redirect()->back();
        }

        $model = Model::findOrFail( $id_paket_wisata );

        $model->fill( $request->input() );
        $model->save();

        flash('Data Berhasil disimpan')->success();

        return redirect()->back()->withInput();
    }

    /**
     * Destroy
     */
    public function destroy( $id_paket_wisata, Request $request )
    {   
        if ( $request->user()->role != 'administrator' ) {
            flash('Hanya admin yang dapat melakukan perubahan.')->error();
            return redirect()->back();
        }

        $model = Model::findOrFail( $id_paket_wisata );
        $model->delete();
        flash('Data Berhasil Dihapus')->success();
        return redirect()->route('admin.kendaraan.index');
    }
}
