<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Driver as Model;

class DriverController extends Controller
{
    /**
     * Index
     */
    public function index()
    {
        $items = Model::paginate(15);
        return view('admin.driver.index', compact('items') );
    }

    /**
     * Create
     */
    public function create()
    {
        $model = new Model();
        return view('admin.driver.create', compact('model') ); 
    }

    /**
     * Store
     */
    public function store( Request $request )
    {
        if ( $request->user()->role != 'administrator' ) {
            flash('Hanya admin yang dapat melakukan perubahan.')->error();
            return redirect()->back();
        }
        
        $paket_wisata = Model::create( $request->input() );
        flash('Data Berhasil disimpan')->success();
        return redirect()->back()->withInput();
    }

    /**
     * Edit
     */
    public function edit( $id_paket_wisata, Request $request )
    {
        $model = Model::findOrFail( $id_paket_wisata );
        return view('admin.driver.edit', compact('model') );
    }

    /**
     * Update
     */
    public function update( $id_paket_wisata, Request $request )
    {
        if ( $request->user()->role != 'administrator' ) {
            flash('Hanya admin yang dapat melakukan perubahan.')->error();
            return redirect()->back();
        }

        $model = Model::findOrFail( $id_paket_wisata );

        $model->fill( $request->input() );
        $model->save();

        flash('Data Berhasil disimpan')->success();

        return redirect()->back()->withInput();
    }

    /**
     * Destroy
     */
    public function destroy( $id_paket_wisata, Request $request )
    {   
        if ( $request->user()->role != 'administrator' ) {
            flash('Hanya admin yang dapat melakukan perubahan.')->error();
            return redirect()->back();
        }

        $model = Model::findOrFail( $id_paket_wisata );
        $model->delete();
        flash('Data Berhasil Dihapus')->success();
        return redirect()->route('admin.driver.index');
    }
}
