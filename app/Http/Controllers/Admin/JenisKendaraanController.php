<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\JenisKendaraan as Model;

class JenisKendaraanController extends Controller
{
    /**
     * Index
     */
    public function index()
    {
        $items = Model::paginate(15);
        return view('admin.jenis-kendaraan.index', compact('items') );
    }

    /**
     * Create
     */
    public function create()
    {
        $model = new Model();
        return view('admin.jenis-kendaraan.create', compact('model') ); 
    }

    /**
     * Store
     */
    public function store( Request $request )
    {
        if ( $request->user()->role != 'administrator' ) {
            flash('Hanya admin yang dapat melakukan perubahan.')->error();
            return redirect()->back();
        }

        $model = Model::create( $request->input() );

        if ( $image = $request->file('image') ) {
            $model->image = $image->store('jenis-kendaraan');
            $model->save();
        }

        flash('Data Berhasil disimpan')->success();
        return redirect()->back()->withInput();
    }

    /**
     * Edit
     */
    public function edit( $id_paket_wisata, Request $request )
    {
        $model = Model::findOrFail( $id_paket_wisata );
        return view('admin.jenis-kendaraan.edit', compact('model') );
    }

    /**
     * Update
     */
    public function update( $id_paket_wisata, Request $request )
    {
        if ( $request->user()->role != 'administrator' ) {
            flash('Hanya admin yang dapat melakukan perubahan.')->error();
            return redirect()->back();
        }

        $model = Model::findOrFail( $id_paket_wisata );

        $model->fill( $request->input() );
        $model->save();

        if ( $image = $request->file('image') ) {
            if ( $model->gambar ) {
                Storage::delete( $model->gambar );
            }
            $model->image = $image->store('jenis-kendaraan');
            $model->save();
        }

        flash('Data Berhasil disimpan')->success();

        return redirect()->back()->withInput();
    }

    /**
     * Destroy
     */
    public function destroy( $id_paket_wisata, Request $request )
    {   
        if ( $request->user()->role != 'administrator' ) {
            flash('Hanya admin yang dapat melakukan perubahan.')->error();
            return redirect()->back();
        }

        $model = Model::findOrFail( $id_paket_wisata );
        $model->delete();
        flash('Data Berhasil Dihapus')->success();
        return redirect()->route('admin.jenis-kendaraan.index');
    }
}
