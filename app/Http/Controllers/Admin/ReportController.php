<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Reservasi;
use App\Models\PaketTour;
use App\Models\Kendaraan;
use PDF;
use DB;

class ReportController extends Controller
{
    /**
     * Index
     */
    public function index()
    {
        return view('admin.report.index');
    }

    /**
     * Store
     */
    public function store( Request $request )
    {
        $req = $request->input('req');

        switch ( $req ) {
            case 'report-reservasi-tour':
                return $this->reportReservasiTour( $request );
                break;

            case 'report-paket-tour':
                return $this->reportPaketTour( $request );
                break;
            case 'report-kendaraan':
                return $this->reportKendaraan( $request );
                break;
            
            default:
                # code...
                break;
        }   
    }

    private function reportReservasiTour( &$request )
    {
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');

        $reservasi = Reservasi::whereNotNull('paket_tour_id')
        ->with(['user', 'paketTour', 'driver', 'kendaraan', 'statusReservasi'])
        ->whereBetween('tanggal_pickup', [ $start_date, $end_date ])
        ->get();


        $pdf = PDF::loadView('pdf.report.reservasi', compact('reservasi', 'start_date', 'end_date'), [], ['format' => 'A4-L'] )->download('report-reservasi.pdf');
    }

    private function reportPaketTour( &$request )
    {
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');

        $paket_wisata = PaketTour::withCount([
            'reservasi as jumlah_reservasi' => function( $q ) use ( $start_date, $end_date ) {
                return $q->where('status_reservasi_id', 5)->whereBetween('tanggal_pickup', [ $start_date, $end_date ]);
            },
            'reservasi as total_pembayaran' => function( $q ) use ( $start_date, $end_date ){
                return $q->select(DB::raw('sum(total)'))->where('status_reservasi_id', 5)->whereBetween('tanggal_pickup', [ $start_date, $end_date ]);
            }
        ])->get();

        $pdf = PDF::loadView('pdf.report.paket-tour', compact('paket_wisata', 'start_date', 'end_date'), [], ['format' => 'A4-L'] )->download('report-paket-tour.pdf');
    }

    private function reportKendaraan( &$request )
    {
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');

        $kendaraan = Kendaraan::withCount([
            'reservasi as jumlah_reservasi' => function( $q ) use ( $start_date, $end_date ) {
                return $q->where('status_reservasi_id', 5)->whereBetween('tanggal_pickup', [ $start_date, $end_date ]);
            },
        ])->with(['jenisKendaraan'])->get();

        $pdf = PDF::loadView('pdf.report.kendaraan', compact('kendaraan', 'start_date', 'end_date'), [], ['format' => 'A4-L'] )->download('report-kendaraan.pdf');
    }

}
