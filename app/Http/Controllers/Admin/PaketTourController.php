<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PaketTour as Model;
use Illuminate\Support\Facades\Storage;

class PaketTourController extends Controller
{
    /**
     * Index
     */
    public function index()
    {
        $items = Model::paginate(15);
        return view('admin.paket-tour.index', compact('items') );
    }

    /**
     * Create
     */
    public function create()
    {
        $model = new Model();
        return view('admin.paket-tour.create', compact('model') ); 
    }

    /**
     * Store
     */
    public function store( Request $request )
    {
        if ( $request->user()->role != 'administrator' ) {
            flash('Hanya admin yang dapat melakukan perubahan.')->error();
            return redirect()->back();
        }

        $model = Model::create( $request->input() );

        if ( $image = $request->file('image') ) {
            $model->image = $image->store('paket-tour');
            $model->save();
        }

        if( $harga_paket_wisata = $request->input('harga_paket_wisata') ) {
            $model->hargaPaketTour()->createMany( $request->input( 'harga_paket_wisata' ) );
        }

        flash('Data Berhasil disimpan')->success();
        return redirect()->route('admin.paket-tour.edit', $model->id);
    }

    /**
     * Edit
     */
    public function edit( $id_paket_wisata, Request $request )
    {
        $model = Model::with(['hargaPaketTour'])->findOrFail( $id_paket_wisata );
        return view('admin.paket-tour.edit', compact('model') );
    }

    /**
     * Update
     */
    public function update( $id_paket_wisata, Request $request )
    {
        if ( $request->user()->role != 'administrator' ) {
            flash('Hanya admin yang dapat melakukan perubahan.')->error();
            return redirect()->back();
        }

        $model = Model::findOrFail( $id_paket_wisata );

        $model->fill( $request->input() );
        $model->save();

        if( $harga_paket_wisata = $request->input('harga_paket_wisata') ) {
            $model->hargaPaketTour()->delete();
            $model->hargaPaketTour()->createMany( $request->input( 'harga_paket_wisata' ) );
        }

        if ( $image = $request->file('image') ) {
            if ( $model->gambar ) {
                Storage::delete( $model->gambar );
            }
            $model->image = $image->store('paket-tour');
            $model->save();
        }

        flash('Data Berhasil disimpan')->success();

        return redirect()->back()->withInput();
    }

    /**
     * Destroy
     */
    public function destroy( $id_paket_wisata, Request $request )
    {   
        if ( $request->user()->role != 'administrator' ) {
            flash('Hanya admin yang dapat melakukan perubahan.')->error();
            return redirect()->back();
        }

        $model = Model::findOrFail( $id_paket_wisata );
        $model->delete();
        flash('Data Berhasil Dihapus')->success();
        return redirect()->route('admin.paket-tour.index');
    }
}
