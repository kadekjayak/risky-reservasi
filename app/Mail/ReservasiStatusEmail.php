<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReservasiStatusEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $reservasi;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $reservasi )
    {
        $this->reservasi = $reservasi;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'level' => 'success',
            'introLines' => [
                sprintf('Status Reservasi dengan nomor **#%s** telah berubah menjadi **%s**', $this->reservasi->kode_reservasi, $this->reservasi->statusReservasi->name)
            ],
            'outroLines' => []
        ];
        return $this->subject( sprintf('Status Reservasi #%s', $this->reservasi->kode_reservasi) )->markdown('reservasi.status-changed', $data);
    }
}
