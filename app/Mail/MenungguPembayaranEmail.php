<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Rekening;

class MenungguPembayaranEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $reservasi;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $reservasi )
    {
        $this->reservasi = $reservasi;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'level' => 'success',
            'introLines' => [
                sprintf('Segera lakukan konfirmasi pembayaran untuk reservasi nomor: **#%s**', $this->reservasi->kode_reservasi ),
                sprintf('Pembayaran dapat dilakukan dengan melakukan transfer sebesar *%s* ke salah satu rekening berikut: ', $this->reservasi->getFormatedTotal())
            ],
            'outroLines' => [],
            'rekenings' => Rekening::all(),
            'actionText' => 'Konfirmasi Pembayaran',
            'actionUrl' => url('konfirmasi-pembayaran'),
        ];
        return $this->subject( sprintf('Menunggu Pembayaran #%s', $this->reservasi->kode_reservasi) )->markdown('reservasi.menunggu-pembayaran', $data);
    }
}
