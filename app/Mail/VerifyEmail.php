<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $user )
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'level' => 'success',
            'introLines' => [
                'Klik link dibawah ini untuk mengaktifkan akun anda'
            ],
            'actionText' => 'Verifikasi Email',
            'actionUrl' => url('verify-email') . '?token=' . $this->user->verification_token,
            'outroLines' => []
        ];
        return $this->markdown('auth.verify', $data);
    }
}
