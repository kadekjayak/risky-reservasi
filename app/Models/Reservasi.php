<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Helper;

class Reservasi extends Model
{  
    public $fillable = ['name', 'alamat_pickup', 'jumlah_penumpang', 'tanggal_pickup', 'nomor_hp', 'email', 'jenis_kendaraan_id', 'paket_tour_id', 'status_reservasi_id', 'driver_id', 'harga_paket_tour', 'harga_jenis_kendaraan', 'kendaraan_id', 'waktu_selesai'];

    public static function boot()
    {
        parent::boot();

        self::creating(function(&$model){
            $model->total = $model->harga_paket_tour + $model->harga_jenis_kendaraan;

            return $model;
        });

        self::updating(function(&$model){
            $model->total = $model->harga_paket_tour + $model->harga_jenis_kendaraan;

            return $model;
        });
    }

    public function paketTour()
    {
        return $this->belongsTo('App\Models\PaketTour');
    }

    public function jenisKendaraan()
    {
        return $this->belongsTo('App\Models\JenisKendaraan');
    }

    public function kendaraan()
    {
        return $this->belongsTo('App\Models\Kendaraan');
    }
    
    public function driver()
    {
        return $this->belongsTo('App\Models\Driver');
    }

    public function statusReservasi()
    {
        return $this->belongsTo('App\Models\StatusReservasi');
    }

    public function konfirmasiPembayaran()
    {
        return $this->hasMany('App\Models\KonfirmasiPembayaran');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getFormatedTotal()
    {
        return Helper::formatMoney( $this->total );
    }
}
