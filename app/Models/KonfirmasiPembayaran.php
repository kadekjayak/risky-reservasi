<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class KonfirmasiPembayaran extends Model
{
    public $fillable = ['reservasi_id', 'nama_pengirim', 'tanggal_transfer', 'jumlah_transfer', 'rekening_id', 'image'];

    public function getImageUrl()
    {
        return Storage::url( $this->image );
    }
}
