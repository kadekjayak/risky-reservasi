<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HargaPaketTour extends Model
{
    public $fillable = ['jumlah_penumpang' ,'price', 'paket_tour_id'];

    public $table = 'harga_paket_tour';

    public function paketTour()
    {
        return $this->belongsTo('App\Models\PaketTour');
    }

    public function getFormatedPrice()
    {
        return 'Rp. ' . number_format( $this->price, 0, ',', '.');
    }
}
