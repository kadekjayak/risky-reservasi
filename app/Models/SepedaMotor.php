<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SepedaMotor extends Model
{
    public $fillable = ['kode_motor', 'nama_motor', 'jenis_motor', 'merk_motor', 'cc', 'warna', 'harga', 'tanggal_launching'];

    public $timestamps = false;

    protected $primaryKey = 'kode_motor';

    protected $table = 'sepeda_motor';

    public $incrementing = false;

}
