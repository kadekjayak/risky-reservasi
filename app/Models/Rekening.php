<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Reservasi;

class Rekening extends Model
{
    public $fillable = ['name', 'account_number', 'account_name'];

    public $timestamps = false;

    public $table = 'rekening';
}
