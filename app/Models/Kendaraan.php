<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Reservasi;

class Kendaraan extends Model
{
    public $fillable = ['nomor_polisi', 'warna', 'tahun', 'jenis_kendaraan_id'];

    public $table = 'kendaraan';

    public function jenisKendaraan()
    {
        return $this->belongsTo('App\Models\JenisKendaraan');
    }

    public function scopeAvailableAt($query, $date)
    {
        $ids = Reservasi::select('kendaraan_id')->whereNotNull('kendaraan_id')->whereDate('tanggal_pickup', $date)->get()->pluck('kendaraan_id')->toArray();
        return $query->whereNotIn('id', $ids);
    }

    public function reservasi()
    {
        return $this->hasMany('App\Models\Reservasi');
    }
}
