<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    public $fillable = ['name', 'alamat', 'nomor_hp', 'jenis_kelamin', 'tgl_mulai_kerja'];

    public function reservasi()
    {
        return $this->hasMany('App\Models\Reservasi');
    }

    public function kendaraan()
    {
        return $this->belongsToMany('App\Models\Kendaraan', 'reservasis', 'driver_id', 'kendaraan_id');
    }

    public function paketWisata()
    {
        return $this->belongsToMany('App\Models\PaketWisata', 'reservasis', 'driver_id', 'paket_wisata_id');
    }
}
