<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class PaketTour extends Model
{
    public $fillable = ['name' ,'description', 'short_description', 'price', 'price_unit'];

    public $table = 'paket_tour';

    public function reservasi()
    {
        return $this->hasMany('App\Models\Reservasi');
    }

    public function hargaPaketTour()
    {
        return $this->hasMany('App\Models\HargaPaketTour');
    }

    public function getImageUrl()
    {
        return Storage::url( $this->image );
    }

    public function getFormatedPrice()
    {
        return 'Rp. ' . number_format( $this->price, 0, ',', '.');
    }

    public function getFirstFormatedPrice()
    {
        $harga = $this->hargaPaketTour()->orderBy('jumlah_penumpang', 'ASC')->first();

        if( $harga ) {
            return 'Rp. ' . number_format( $harga->price, 0, ',', '.');
        }

        return null;
    }


    public function getPrice( $jumlah_penumpang = 1 )
    {   
        $harga_paket = $this->hargaPaketTour()
                                ->where('jumlah_penumpang', '>=', $jumlah_penumpang)
                                ->orderBy('jumlah_penumpang', 'ASC')
                                ->first();
        if ( ! $harga_paket ) {
            if( $jumlah_penumpang == 1 ) {
                $harga_paket = $this->hargaPaketTour()->orderBy('jumlah_penumpang', 'ASC')->first();
            } else {
                abort('422', 'Paket dengan jumlah penumpang tersebut tidak tersedia');
            }   
        }

        return $harga_paket->price;
    
    }
}
