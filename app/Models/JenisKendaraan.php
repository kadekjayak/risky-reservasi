<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class JenisKendaraan extends Model
{
    public $fillable = ['name' ,'description', 'short_description', 'price', 'seat'];

    public function kendaraan()
    {
        return $this->hasMany('App\Models\Kendaraan');
    }

    public function getImageUrl()
    {
        return Storage::url( $this->image );
    }

    public function getFormatedPrice()
    {
        return 'Rp. ' . number_format( $this->price, 0, ',', '.');
    }

    public function getNameWithPriceAttribute()
    {
        return $this->name . ' (' . $this->getFormatedPrice() . ' )';
    }
}
