<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::resource('paket-tour', 'PaketTourController');
Route::resource('paket-tour.harga', 'PaketTourHargaController');
Route::resource('jenis-kendaraan', 'JenisKendaraanController');
Route::resource('kendaraan', 'JenisKendaraanController');
Route::resource('reservasi', 'ReservasiController');
Route::resource('konfirmasi-pembayaran', 'KonfirmasiPembayaranController');
Route::resource('migrate', 'Admin\MigrateController');

Route::name('admin.')->prefix('admin')->namespace('Admin')->middleware('admin')->group(function(){
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::resource('paket-tour', 'PaketTourController');
    Route::resource('jenis-kendaraan', 'JenisKendaraanController');
    Route::resource('kendaraan', 'KendaraanController');
    Route::resource('driver', 'DriverController');
    Route::resource('users', 'UserController');
    Route::resource('rekening', 'RekeningController');
    Route::resource('admin', 'AdminController');
    Route::resource('reservasi', 'Reservasi\ReservasiController');
    Route::resource('konfirmasi-pembayaran', 'KonfirmasiPembayaranController');
    Route::resource('reports', 'ReportController');
    Route::resource('sepeda-motor', 'SepedaMotorController');
});

Route::name('akun.')->prefix('akun')->middleware('auth')->group(function(){
    Route::resource('reservasi', 'ReservasiController');
});
Route::resource('akun', 'UserController');


//Route::group(function(){
    Route::get('login-admin', 'Auth\LoginAdminController@showLoginForm');
    Route::post('login-admin', 'Auth\LoginAdminController@login');
//});

Route::get('verify-email', 'Auth\VerifyEmailController@index');