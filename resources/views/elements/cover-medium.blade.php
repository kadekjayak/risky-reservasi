<section id="cover" class="overlay-dark overlay-darken py-5 justify-content-center" style="background-size: cover; background-image: url('{{ @$background ?: url('/img/cover-tour.jpg') }}')">
    <div class="container text-center text-white" style="position: relative; z-index: 1;">
        <div class="pt-5">
            <h1 class="text-white">{{ $title }}</h1>
            <div class="divider short p-2"></div>
            @if( @$subtitle)
                <p>
                    {{ $subtitle }}
                </p>
            @endif
        </div>
    </div>
</section>