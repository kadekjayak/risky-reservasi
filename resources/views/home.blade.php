@extends('layouts.app')

@section('content')

<section id="cover" class="mb-5 overlay-dark" style="background-image: url('{{ url('/img/cover.jpg') }}')" class="vh-100">
    <div class="container text-center text-white vh-100 d-flex align-items-center" style="position: relative; z-index: 1;">
        <div class=" text-white">
            <h1 class="text-white">A very warm welcome to the luxury of tour & travel with Travellink Bali</h1>
            <div class="divider short p-4"></div>
            <p>
                We will provide you with the unique opportunity to explore the real Bali. 
                You will find out more about the beautiful Balinese people, their culture and customs as well as the intriguing countryside. 
                You will have the most wonderful time exploring Bali with the help of Travelink Bali Tour Services.
            </p>
        </div>
    </div>
</section>

<section id="advantages" class="mb-5 py-5">
   <div class="container">
      <div class="row align-items-center">
         <div class="col-md-8">
            <div class="mb-4">
               <div class="media">
                  <i class="fa fa-wallet fa-3x mr-3 text-primary"></i>
                  <div class="media-body">
                     <h5 class="mt-0">No Hidden Cost</h5>
                     The trip is depend on our deal and it’s include car, fuel, driver fee, toll, and all parking fee
                  </div>
               </div>
            </div>
            <div class="mb-4">
               <div class="media">
                  <i class="fa fa-grin-stars fa-3x mr-3 text-primary"></i>
                  <div class="media-body">
                     <h5 class="mt-0">Great Satisfaction</h5>
                     I am a great driver, and patient driver, our car is very comfortable for traveling and has good air conditioner
                  </div>
               </div>
            </div>
            <div class="mb-4">
               <div class="media">
                  <i class="fa fa-hand-holding-usd fa-3x mr-3 text-primary"></i>
                  <div class="media-body">
                     <h5 class="mt-0">Competitive Rates</h5>
                     Our price is reasonable with our perfect services and facilities
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-4 text-center mb-3">
            <img class="img-fluid w-75" src="{{ url('/img/logo.png') }}">
        </div>
      </div>
   </div>
</section>

<section class="bg-light py-5 mb-5">
    <div class="container">
        <h3 class="mb-3">Paket Wisata</h3>
        <div class="row mb-3">
            @foreach( $paket_wisata as $item)
            <div class="col-md-4">
                <div class="card bg-transparent border-0">
                    <a href="{{ route('paket-tour.show', $item->id) }}">
                        <img class="img-fluid mb-2 rounded" src="{{ $item->getImageUrl() }}" />
                    </a>
                    <div class="card-body p-0">
                        <h5 class="">{{ $item->name }}</h5>
                        <p class="small mb-0">{{ $item->getFirstFormatedPrice() }}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="text-center">
            <a href="{{ url('/paket-tour') }}" class="btn btn-primary">
                Lihat Semuanya
            </a>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="mb-5">
            <h3>Kendaraan</h3>
            <div class="row">
                @foreach( $jenis_kendaraan as $item)
                <div class="col-md-3">
                    <div class="card border-0">
                        <a href="{{ route('kendaraan.show', $item->id) }}">
                            <img class="img-fluid mb-2 rounded" src="{{ $item->getImageUrl() }}" />
                        </a>
                        <div class="card-body p-0">
                            <h5 class="mb-0">{{ $item->name }}</h5>
                            <p class="small">{{ $item->getFormatedPrice() }} - {{ $item->seat }} Seat</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

<!--
<section class="py-5">
    <div class="container text-center">
        <h1>Punya pertanyaan?</h1>
        <a href="" class="btn btn-success">
            <i class="fab fa-whatsapp mr-2"></i> +62 811 3854 799
        </a>
    </div>
</section>
-->


<section class="bg-primary py-5 text-white">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-8">
                <p>Bali is a beautiful and interesting place to visit.
                You don’t have to worry about driving or arranging anything, 
                We’ll do it all for you with a warm and friendly smile.</p>
            </div>
            <div class="col-md-4 text-center">
                Any Question?<br>
                <a href="" class="btn btn-success">
                    <i class="fab fa-whatsapp mr-2"></i> +62 811 3854 799
                </a>
            </div>
        </div>
    </div>
</section>
@endsection
