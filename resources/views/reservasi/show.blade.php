@extends('layouts.app')

@section('content')
@include('elements.cover-medium', ['title' => 'Detail Reservasi',  'subtitle' => 'Lakukan pembayaran & konfirmasi pada bagian bawah halaman ini.'])

<section class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-3">
                <article>
                    <p class="mb-0 text-muted">Reservasi</p>
                    <h1 class="mb-3">#{{ $model->kode_reservasi }}</h1>

                    <div class="alert alert-primary mb-4" role="alert">
                        <div class="row text-dark">
                            <div class="col-md-6">
                                <span class="text-muted small">Nama</span>
                                <p class="strong">{{ $model->name }}</p>
                            </div>
                            
                            <div class="col-md-6">
                                <span class="text-muted small">Nomor HP</span>
                                <p>{{ $model->nomor_hp }}</p>
                            </div>
                            <div class="col-md-6">
                                <span class="text-muted small">Email</span>
                                <p>{{ $model->email }}</p>
                            </div>
                            <div class="col-md-6">
                                <span class="text-muted small">Jumlah Penumpang</span>
                                <p>{{ $model->jumlah_penumpang }}</p>
                            </div>
                            <div class="col-md-6">
                                <span class="text-muted small">Tanggal Tour</span>
                                <p class="font-weight-bold">{{ $model->tanggal_pickup }}</p>
                            </div>
                            <div class="col-md-6" class="alamat-pickup">
                                <span class="text-muted small">Alamat Pickup</span>
                                <p>{{ $model->alamat_pickup }}</p>
                            </div>
                        </div>
                    </div>

                    <div class="mb-4">
                        <h5 class="font-weight-bold">Detail Reservasi</h5>

                        @if( $model->paket_tour_id )
                            <div class="media mb-3">
                                <img src="{{ Storage::url($model->paketTour->image) }}" width="100" class="mr-2" >
                                <div class="media-body">
                                    <p class="mb-0 h4">{{ $model->paketTour->name }}</p>
                                    <p>Rp. {{ Helper::formatMoney( $model->harga_paket_tour ) }}</p>
                                </div>
                            </div>
                        @endif

                        @if( $model->jenis_kendaraan_id )
                            <div class="media">
                                <img src="{{ Storage::url($model->jenisKendaraan->image) }}" width="100" class="mr-2" >
                                <div class="media-body">
                                    <p class="mb-0 h4">{{ $model->jenisKendaraan->name }}</p>
                                    <p>Rp. {{ Helper::formatMoney( $model->harga_jenis_kendaraan ) }}</p>
                                </div>
                            </div>
                        @else
                            <div class="media">
                                <img src="{{ url('/img/no-car.png') }}" width="100" class="mr-2" >
                                <div class="media-body">
                                    <p class="mb-0 h4">Kendaraan Sendiri</p>
                                    <p>Agent kami akan menguhubungi anda mengenai penjemputan Guide.</p>
                                </div>
                            </div>
                        @endif
                    </div>

                    <h5>Total: <strong>Rp. {{ $model->getFormatedTotal() }}</strong></h5>
                

                    <h5 class="font-weight-bold mt-4">Lakukan Konfirmasi Pembayaran</h5>
                    <p>Sertakan kode reservasi pada catatan transfer.</p>
                    <p>Klik tombol dibawah ini untuk melakukan Konfirmasi Pembayaran</p>
                    <a class="btn btn-primary" href="{{ route('konfirmasi-pembayaran.index') }}">
                        Konfirmasi Pembayaran
                    </a>
                </article>
            </div>
        </div>
    </div>
</section>
@endsection
