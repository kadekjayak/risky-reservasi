@include('elements.head')

<body>
    <div id="app">
    
        @include('elements.navbar', ['sticky' => false])

        <div class="py-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">

                        <div class="list-group">
                            <a class="list-group-item list-group-item-action {{ (request()->routeIs('admin.dashboard')) ? 'active' : '' }}" href="{{ route('admin.dashboard')}}">
                                Dashboard
                            </a>
                            <a class="list-group-item list-group-item-action {{ (request()->routeIs('admin.reservasi.*')) ? 'active' : '' }}" href="{{ route('admin.reservasi.index')}}">
                                Reservasi
                            </a>
                            <a class="list-group-item list-group-item-action {{ (request()->routeIs('admin.paket-tour.*')) ? 'active' : '' }}" href="{{ route('admin.paket-tour.index')}}">
                                Paket Tour
                            </a>
                            <a class="list-group-item list-group-item-action {{ (request()->routeIs('admin.jenis-kendaraan.*')) ? 'active' : '' }}" href="{{ route('admin.jenis-kendaraan.index')}}">
                                Jenis Kendaraan
                            </a>
                            <a class="list-group-item list-group-item-action {{ (request()->routeIs('admin.kendaraan.*')) ? 'active' : '' }}" href="{{ route('admin.kendaraan.index')}}">
                                Kendaraan
                            </a>
                            <a class="list-group-item list-group-item-action {{ (request()->routeIs('admin.driver.*')) ? 'active' : '' }}" href="{{ route('admin.driver.index')}}">
                                Driver
                            </a>
                            <a class="list-group-item list-group-item-action {{ (request()->routeIs('admin.konfirmasi-pembayaran.*')) ? 'active' : '' }}" href="{{ route('admin.konfirmasi-pembayaran.index')}}">
                                Konfirmasi Pembayaran
                            </a>
                            <a class="list-group-item list-group-item-action {{ (request()->routeIs('admin.users.*')) ? 'active' : '' }}" href="{{ route('admin.users.index')}}">
                                Users
                            </a>
                            <a class="list-group-item list-group-item-action {{ (request()->routeIs('admin.rekening.*')) ? 'active' : '' }}" href="{{ route('admin.rekening.index')}}">
                                Rekening
                            </a>
                            <a class="list-group-item list-group-item-action {{ (request()->routeIs('admin.admin.*')) ? 'active' : '' }}" href="{{ route('admin.admin.index')}}">
                                Admin
                            </a>
                        
                            <a class="list-group-item list-group-item-action {{ (request()->routeIs('admin.report.*')) ? 'active' : '' }}" href="{{ route('admin.reports.index')}}">
                                Report
                            </a>
                            
                        </div>
                    </div>
                    <div class="col-md-9">
                        <main>
                            @include('flash::message')
                            @yield('content')
                        </main>
                    </div>
                </div>
            </div>

        </div>
       
    </div>

    @include('elements.scripts')
</body>
</html>
