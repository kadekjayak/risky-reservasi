@include('elements.head')
    <body>
        <div id="app">
            @include('elements.navbar')
            @include('elements.cover-medium', ['title' => 'Dashboard'])

            <div class="container py-5">
                <div class="row">
                    <div class="col-md-3">
                        <div class="list-group">
                            <a href="{{ route('akun.reservasi.index') }}" class="list-group-item list-group-item-action">
                                <i class="fa fa-calendar mr-1"></i> Reservasi
                            </a>
                            <a href="{{ route('akun.index') }}" class="list-group-item list-group-item-action">
                                <i class="fa fa-user mr-1"></i> Informasi Akun
                            </a>
                            <a href="#" class="list-group-item list-group-item-action">
                                <i class="fa fa-sign-out-alt mr-1"></i> Logout
                            </a>
                        </div>
                    </div>
                    <div class="col-md-8">
                        @yield('content')
                    </div>
                </div>
            </div>
            @include('elements.footer')
        </div>

        @include('elements.scripts')
    </body>
</html>
