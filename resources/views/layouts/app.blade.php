@include('elements.head')
    <body>
        <div id="app">
            @include('elements.navbar')
            @yield('content')
            @include('elements.footer')
        </div>

        @include('elements.scripts')
    </body>
</html>
