@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        Tambah Sepeda Motor
    </div>
    <div class="card-body">
        @include('admin.sepeda-motor.form')
    </div>
</div>

@endsection
