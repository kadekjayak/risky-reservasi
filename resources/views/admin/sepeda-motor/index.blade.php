@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        Sepeda Motor
        @if( auth()->user()->role == 'administrator')
            <a class="btn btn-primary" href="{{ route('admin.sepeda-motor.create') }}">Tambah Baru</a>
        @endif
    </div>

    <div class="card-body">
        <table class="table">
            <thead>
                <th>Kode Motor</th>
                <th>Nama</th>
                <th>Jenis</th>
                <th>Merk</th>
                <th>CC</th>
                <th>Warna</th>
                <th>Harga</th>
                <th>Tanggal Launching</th>
                <th></td>
            </thead>
            <tbody>
                @foreach( $items as $item)
                    <tr>
                        <td>
                            <?php echo $item->kode_motor; ?>
                        </td>
                        <td>
                            <?php echo $item->nama_motor; ?>
                        </td>
                        <td>
                            <?php echo $item->jenis_motor; ?>
                        </td>
                        <td>
                            <?php echo $item->merk_motor; ?>
                        </td>
                        <td>
                            <?php echo $item->cc; ?>
                        </td>
                        <td>
                            <?php echo $item->harga; ?>
                        </td>
                        <td>
                            <?php echo $item->warna; ?>
                        </td>
                        <td>
                            <?php echo $item->tanggal_launching; ?>
                        </td>
                        <td width="150">
                            

                            {{ Form::model( $item, [
                                'route' => [ 
                                        'admin.sepeda-motor.destroy',
                                        $item->kode_motor
                                    ], 
                                'method' => 'DELETE' 
                            ]) }}
                                <a class="btn btn-primary btn-sm" href="<?php echo route('admin.sepeda-motor.edit', ['kode_motor' => $item->kode_motor]); ?>">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <button type="submit" class="btn btn-sm btn-danger">
                                    <i class="fa fa-trash"></i>
                                </button>
                            {{ Form::close() }}
                        
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection
