@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        Edit Sepeda Motor
        {{ Form::model( $model, [
                'route' => [ 
                        'admin.sepeda-motor.destroy', 
                        $model->kode_motor
                    ], 
                'method' => 'DELETE',
                'class'  => 'form-confirm-delete'
            ]) }}
            <button class="btn btn-sm btn-danger">
                <i class="fa fa-trash"></i>
            </button>
        {{ Form::close() }}
    </div>
    <div class="card-body">
        @include('admin.sepeda-motor.form', compact('model'))
    </div>
</div>

@endsection
