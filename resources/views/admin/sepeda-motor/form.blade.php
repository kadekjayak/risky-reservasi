{{ Form::model( $model, [
                    'route' => [ 
                            ! $model->kode_motor ? 'admin.sepeda-motor.store' : 'admin.sepeda-motor.update', 
                            $model->kode_motor
                        ], 
                    'method' => $model->kode_motor ? 'PUT' : 'POST' 
                ]) }}
    <div class="form-group">  
        <label>Kode Motor</label>
        {{ Form::text('kode_motor', null, ['class' => 'form-control', 'placeholder' => 'Kode Motor']) }}
    </div>
    <div class="form-group">  
        <label>Nama Motor</label>
        {{ Form::text('nama_motor', null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group">  
        <label>Jenis Motor</label>
        {{ Form::select('jenis_motor', $jenis_motors, null, ['class' => 'form-control', 'placeholder' => 'Jenis Motor']) }}
    </div>
    <div class="form-group">  
        <label>Merk Motor</label>
        {{ Form::select('merk_motor', $merk_motors, null, ['class' => 'form-control', 'placeholder' => 'Merk Motor']) }}
    </div>
    <div class="form-group">  
        <label>Harga</label>
        {{ Form::text('harga', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        <label>CC</label>
        {{ Form::number('cc', null, ['class' => 'form-control', 'placeholder' => 'CC']) }}
    </div>

    <div class="form-group">
        <label>Warna</label>
        {{ Form::text('warna', null, ['class' => 'form-control', 'placeholder' => 'Warna']) }}
    </div>

    <div class="form-group">
        <label>Tanggal Launching</label>
        {{ Form::date('tanggal_launching', null, ['class' => 'form-control', 'placeholder' => 'Tanggal Launching']) }}
    </div>

    <button type="submit" class="btn btn-primary">Simpan</button>
{{ Form::close() }}