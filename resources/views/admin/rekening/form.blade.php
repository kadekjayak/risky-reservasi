{{ Form::model( $model, [
                    'route' => [ 
                            ! $model->id ? 'admin.rekening.store' : 'admin.rekening.update', 
                            $model->id
                        ], 
                    'enctype' => 'multipart/form-data',
                    'method' => $model->id ? 'PUT' : 'POST' 
                ]) }}
    <div class="form-group">  
        <label>Nama Bank</label>
        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nama Bank']) }}
    </div>

    <div class="form-group">
        <label>Nama Pemilik Rekening</label>
        {{ Form::text('account_name', null, ['class' => 'form-control', 'placeholder' => 'Nama Pemilik Rekening']) }}
    </div>

    <div class="form-group">
        <label>Nomor Rekening</label>
        {{ Form::text('account_number', null, ['class' => 'form-control', 'placeholder' => 'Nomor Rekening']) }}
    </div>

    <button type="submit" class="btn btn-primary">Simpan</button>
{{ Form::close() }}