@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        Rekening Baru
    </div>
    <div class="card-body">
        @include('admin.rekening.form')
    </div>
</div>

@endsection
