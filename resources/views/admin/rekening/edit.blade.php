@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        Edit Rekening
        {{ Form::model( $model, [
                'route' => [ 
                        'admin.rekening.destroy', 
                        $model->id
                    ], 
                'method' => 'DELETE',
                'class'  => 'form-confirm-delete'
            ]) }}
            <button class="btn btn-sm btn-danger">
                <i class="fa fa-trash"></i>
            </button>
        {{ Form::close() }}
    </div>
    <div class="card-body">
        @include('admin.rekening.form')
    </div>
</div>

@endsection
