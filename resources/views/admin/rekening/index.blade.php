@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        Rekening
        @if( auth()->user()->role == 'administrator')
            <a class="btn btn-primary" href="{{ route('admin.rekening.create') }}">Tambah Baru</a>
        @endif
    </div>
    

    <div class="card-body">
        
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Bank</th>
                    <th>Pemilik</th>
                </tr>
            </thead>
            <tbody>
                @foreach( $items as $item)
                    <tr>
                        <td>
                            <a href="{{ route('admin.rekening.edit', $item->id) }}">
                                {{ $item->id }}
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('admin.rekening.edit', $item->id) }}">
                                {{ $item->name }}<br>
                                <small>{{ $item->account_number}}</small>
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('admin.rekening.edit', $item->id) }}">
                                {{ $item->account_name }}
                            </a>
                        </td>
                    </tr>
                @endforeach
                
            </tbody>
        </table>

    </div>
</div>

@endsection
