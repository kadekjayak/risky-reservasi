{{ Form::model( $model, [
                    'route' => [ 
                            ! $model->id ? 'admin.admin.store' : 'admin.admin.update', 
                            $model->id
                        ], 
                    'method' => $model->id ? 'PUT' : 'POST' 
                ]) }}
    <div class="form-group">  
        <label>Role</label>
        {{ Form::select('role', ['user' => 'User', 'admin' => 'Admin', 'owner' => 'Owner'], null, ['class' => 'form-control', 'placeholder' => 'Pilih Level User', 'required' => true]) }}
    </div>
    <div class="form-group">  
        <label>Nama</label>
        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nama']) }}
    </div>
    <div class="form-group">  
        <label>Email</label>
        {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) }}
    </div>
    
    <hr>

    <div class="form-group">
        <label>Password</label>
        {{ Form::password('password', ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        <label>Konfirmasi Password</label>
        {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
    </div>

    <button type="submit" class="btn btn-primary">Simpan</button>
{{ Form::close() }}