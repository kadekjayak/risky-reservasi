@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        Admin
    </div>
    <div class="card-body">
        @include('admin.admin.form')
    </div>
</div>

@endsection
