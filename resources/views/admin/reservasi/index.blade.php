@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        Reservasi
        @if( auth()->user()->role == 'administrator')
            <a class="btn btn-primary" href="{{ route('admin.reservasi.create') }}">Tambah Baru</a>
        @endif
    </div>

    <div class="card-body">

        <form>
            <div class="row mb-4 d-flex flex-row">
                <div class="col-6">
                    <input type="text" name="search" class="form-control" value="{{ request()->query('search') }}">
                </div>
                <div class="col-5">
                    {{ Form::select('status_reservasi_id', [null => 'Status Reservasi'] + $status_reservasi->toArray(), null, ['class' => 'form-control'])}}
                </div>
                <div class="">
                    <button class="btn btn-primary">
                        Cari
                    </button>
                </div>
            </div>
        </form>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Nama</th>
                    <th>Penumpang</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach( $items as $item)
                    <tr>
                        <td>
                            <a href="{{ route('admin.reservasi.show', $item->id) }}">
                                {{ $item->tanggal_pickup }}
                            </a>
                        </td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->jumlah_penumpang }}</td>
                        <td>{{ $item->statusReservasi->name }}</td>
                        <td>
                            @if( auth()->user()->role == 'administrator')
                            <a href="{{ route('admin.reservasi.edit', $item->id) }}">
                                <i class="fa fa-edit"></i>
                            </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                
            </tbody>
        </table>
    </div>
</div>

@endsection
