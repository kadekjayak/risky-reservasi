@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        Reservasi
    </div>
    <div class="card-body">
        <p class="mb-0 text-muted">Reservasi</p>
        <h1 class="mb-3">#{{ $model->kode_reservasi }}</h1>

        <div class="alert alert-primary mb-4" role="alert">
            <div class="row text-dark">
                <div class="col-md-6">
                    <span class="text-muted small">Jenis Kendaraan yang Diminta</span>
                    <p class="strong">{{ @$model->jenisKendaraan ? $model->jenisKendaraan->name : "Kendaraan Customer" }}</p>
                </div>
                <div class="col-md-6">
                    <span class="text-muted small">Status Reservasi</span>
                    {{ Form::select( 'status_reservasi_id', $status_reservasi->toArray(), $model->status_reservasi_id, ['disabled' => auth()->user()->role != 'administrator', 'class' => 'form-control listen', 'id' => 'status-reservasi', 'data-url' => route('admin.reservasi.update', $model->id)]) }}
                </div>
                <div class="col-md-6">
                    <span class="text-muted small">Kendaraan</span>
                    <p class="strong">{{ @$model->kendaraan ? $model->kendaraan->nomor_polisi : '-' }}</p>
                </div>
                @if( $model->jenis_kendaraan_id )
                    <div class="col-md-6">
                        <span class="text-muted small d-block">Driver</span>
                        {{ Form::select( 'driver_id', [null => 'Pilih Driver'] + $drivers->toArray(), $model->driver_id, ['disabled' => auth()->user()->role != 'administrator','class' => 'form-control listen', 'id' => 'driver-reservasi', 'data-url' => route('admin.reservasi.update', $model->id)]) }}
                    </div>
                @endif
            </div>
        </div>
        
        <hr>

        <div class="mb-4">
            <h5 class="font-weight-bold">Detail Reservasi</h5>

            @if( $model->paket_tour_id )
                <div class="media mb-3">
                    <img src="{{ Storage::url($model->paketTour->image) }}" width="100" class="mr-2" >
                    <div class="media-body">
                        <p class="mb-0 h4">{{ $model->paketTour->name }}</p>
                        <p>Rp. {{ Helper::formatMoney( $model->harga_paket_tour ) }}</p>
                    </div>
                </div>
            @endif

            @if( $model->jenis_kendaraan_id )
                <div class="media">
                    <img src="{{ Storage::url($model->jenisKendaraan->image) }}" width="100" class="mr-2" >
                    <div class="media-body">
                        <p class="mb-0 h4">{{ $model->jenisKendaraan->name }}</p>
                        <p>Rp. {{ Helper::formatMoney( $model->harga_jenis_kendaraan ) }}</p>
                    </div>
                </div>
            @else
                <div class="media">
                    <img src="{{ url('/img/no-car.png') }}" width="100" class="mr-2" >
                    <div class="media-body">
                        <p class="mb-0 h4">Kendaraan Sendiri</p>
                        <p>Agent kami akan menguhubungi anda mengenai penjemputan Guide.</p>
                    </div>
                </div>
            @endif
        </div>

        <h5>Total: <strong>Rp. {{ $model->getFormatedTotal() }}</strong></h5>
    </div>
</div>

@endsection
