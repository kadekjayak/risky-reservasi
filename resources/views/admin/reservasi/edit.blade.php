@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        Edit Reservasi
    </div>
    <div class="card-body">
        @include('admin.reservasi.form', compact('model'))
    </div>
</div>

@endsection
