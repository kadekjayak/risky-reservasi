{{ Form::model( $model, [
                    'id' => 'Form-Reservasi',
                    'route' => [ 
                            ! $model->id ? 'admin.reservasi.store' : 'admin.reservasi.update', 
                            $model->id
                        ], 
                    'enctype' => 'multipart/form-data',
                    'method' => $model->id ? 'PUT' : 'POST' 
                ]) }}
    <div class="form-group">  
        <label>Paket Wisata</label>
        {{ Form::select('paket_tour_id', [null => 'Pilih Paket Wisata'] + $paket_wisatas->toArray(), null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">  
        <label>Jenis Kendaraan</label>
        {{ Form::select('jenis_kendaraan_id', [null => 'Pilih Jenis Kendaraan'] + $jenis_kendaraans->pluck('name_with_price', 'id')->toArray(), null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">  
        <label>Kendaraan</label>
        {{ Form::select('kendaraan_id', [null => 'Pilih Jenis Kendaraan'] + $kendaraans->pluck('nomor_polisi', 'id')->toArray(), null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">  
        <label>Driver</label>
        {{ Form::select('driver_id', [null => 'Pilih Driver'] + $drivers->toArray(), null, ['class' => 'form-control']) }}
    </div>

    <hr>

    <div class="form-group">  
        <label>Nama</label>
        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nama Tamu', 'required' => true]) }}
    </div>

    <div class="form-group">  
        <label>Email</label>
        {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Nama Tamu']) }}
    </div>

    <div class="form-group">  
        <label>Nomor HP</label>
        {{ Form::text('nomor_hp', null, ['class' => 'form-control', 'placeholder' => 'Nama Tamu']) }}
    </div>

    <div class="form-group">  
        <label>Jumlah Penumpang</label>
        {{ Form::number('jumlah_penumpang', null, ['class' => 'form-control']) }}
    </div>

    <hr>

    <div class="form-group">  
        <label>Tanggal Tour</label>
        {{ Form::date('tanggal_pickup', null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group">  
        <label>Alamat Pickup</label>
        {{ Form::textarea('alamat_pickup', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">  
        <label>Status Reservasi</label>
        {{ Form::select('status_reservasi_id', [null => 'Pilih Status Reservasi'] + $status_reservasi->toArray(), null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">  
        <label>Waktu Selesai</label>
        {{ Form::text('waktu_selesai', null, [ 'class' => 'datetimepicker form-control']) }}
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label>Harga Kendaraan</label>
                {{ Form::number('harga_jenis_kendaraan', null, ['class' => 'form-control', 'placeholder' => '', 'required' => true]) }}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Harga Paket Wisata</label>
                {{ Form::number('harga_paket_tour', null, ['class' => 'form-control', 'placeholder' => '', 'required' => true]) }}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Total</label>
                {{ Form::number('total', null, ['class' => 'form-control', 'readonly' => true]) }}
            </div>
        </div>
    </div>

    <button type="submit" class="btn btn-primary">Simpan</button>
{{ Form::close() }}