@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        Supir
        @if( auth()->user()->role == 'administrator')
            <a class="btn btn-primary" href="{{ route('admin.driver.create') }}">Tambah Baru</a>
        @endif
    </div>

    <div class="card-body">
        <div class="row">
            @foreach( $items as $item)
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <a href="{{ url(route('admin.driver.edit', $item->id)) }}">
                                <h4 class="h5 mb-0">{{ $item->name }}</h4>
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

@endsection
