{{ Form::model( $model, [
                    'route' => [ 
                            ! $model->id ? 'admin.paket-tour.store' : 'admin.paket-tour.update', 
                            $model->id
                        ], 
                    'enctype' => 'multipart/form-data',
                    'method' => $model->id ? 'PUT' : 'POST' 
                ]) }}
    <div class="form-group">  
        <label>Nama Paket Tour</label>
        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nama Paket Wisata']) }}
    </div>

    <div class="form-group">
        <label>Deskripsi</label>
        {{ Form::textarea('description', null, ['class' => 'editor', 'placeholder' => 'Deskripsi']) }}
    </div>


    <div class="form-group">
        <label>Deskripsi Pendek</label>
        {{ Form::textarea('short_description', null, ['class' => 'form-control', 'placeholder' => 'Deskripsi Pendek']) }}
    </div>

    <div class="form-group">
        <label>Gambar</label>
        
        @if( $model->image )
            <div class="d-block">
                <img src="{{ Storage::url( $model->image ) }}" class="img-fluid mb-3" width="150">
            </div>
        @endif

        {{ Form::file('image') }}
    </div>

    <hr>

    <h5>Harga Paket Wisata</h5>
    <div id="Tabel-Harga-Wisata">
        <table class="table">
            <thead>
                <tr>
                    <th></th>
                    <th>Jumlah Penumpang</th>
                    <th>Harga</th>
                </tr>
            </thead>
            <tbody>
                @if( $model->hargaPaketTour )
                    @foreach( $model->hargaPaketTour as $index => $harga )
                        <tr>
                            <th>
                                <button class="btn btn-sm btn-danger action-delete"><i class="fa fa-trash"></i></button>
                            </th>
                            <th>
                                <input type="number" min="1" required class="form-control" value="{{ $harga->jumlah_penumpang }}" name="harga_paket_wisata[{{ $index }}][jumlah_penumpang]">
                            </th>
                            <th>
                                <input type="number" min="1" required class="form-control" value="{{ $harga->price }}" name="harga_paket_wisata[{{ $index }}][price]">
                            </th>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        <div class="text-center">
            <button class="btn btn-primary action-add-price">Tambah Harga</button>
        </div>
    </div>

    <hr>

    <button type="submit" class="btn btn-primary">Simpan</button>
{{ Form::close() }}