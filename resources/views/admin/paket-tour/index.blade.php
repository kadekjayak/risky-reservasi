@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        Paket Tour
        @if( auth()->user()->role == 'administrator')
            <a class="btn btn-primary" href="{{ route('admin.paket-tour.create') }}">Tambah Baru</a>
        @endif
    </div>

    <div class="card-body">
        <div class="row">
            @foreach( $items as $item)
                <div class="col-md-4">
                    <div class="card mb-3">
                        <a href="{{ route('admin.paket-tour.edit', $item->id)}}">
                            <img class="card-img-top" src="{{ Storage::url( $item->image) }}" alt="Card image cap">
                        </a>
                        <div class="card-body">
                            <h4 class="h5 mb-0">{{ $item->name }}</h4>
                            <strong>{{ $item->price }}</strong>

                            <p>{{ $item->short_description }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

@endsection
