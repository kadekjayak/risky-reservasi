@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        Konfirmasi Pembayaran
    </div>
    <div class="card-body">
        <table class="table table-striped">
            <thead>
                <tr>
                    <td>Nama</td>
                    <td>Tanggal</td>
                    <td>Jumlah</td>
                </tr>
            </thead>
            <tbody>
                @foreach( $items as $item)
                    <tr>
                        <td>{{ $item->nama_pengirim }}</td>
                        <td>{{ $item->tanggal_transfer }}</td>
                        <td>{{ $item->jumlah_transfer }}</td>
                        <td>
                            <a target="_blank" href="{{ $item->getImageUrl() }}">
                                <img width="100px" class="img-fluid" src="{{ $item->getImageUrl() }}" />
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
            
    </div>
</div>

@endsection
