@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        Paket Wisata baru
      
    </div>
    <div class="card-body">
        @include('admin.paket-wisata.form')
    </div>
</div>

@endsection
