{{ Form::model( $model, [
                    'route' => [ 
                            ! $model->id ? 'admin.paket-wisata.store' : 'admin.paket-wisata.update', 
                            $model->id
                        ], 
                    'enctype' => 'multipart/form-data',
                    'method' => $model->id ? 'PUT' : 'POST' 
                ]) }}
    <div class="form-group">  
        <label>Nama Paket Wisata</label>
        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nama Paket Wisata']) }}
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Harga</label>
                {{ Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Harga']) }}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Unit Harga</label>
                {{ Form::text('price_unit', null, ['class' => 'form-control', 'placeholder' => 'per 2 Orang']) }}
            </div>
        </div>
    </div>

    <div class="form-group">
        <label>Deskripsi</label>
        {{ Form::textarea('description', null, ['class' => 'editor', 'placeholder' => 'Deskripsi']) }}
    </div>


    <div class="form-group">
        <label>Deskripsi Pendek</label>
        {{ Form::textarea('short_description', null, ['class' => 'form-control', 'placeholder' => 'Deskripsi Pendek']) }}
    </div>

    <div class="form-group">
        <label>Gambar</label>
        
        @if( $model->image )
            <div class="d-block">
                <img src="{{ Storage::url( $model->image ) }}" class="img-fluid mb-3" width="150">
            </div>
        @endif

        {{ Form::file('image') }}
    </div>

    <button type="submit" class="btn btn-primary">Simpan</button>
{{ Form::close() }}