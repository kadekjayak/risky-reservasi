@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        Edit Paket Wisata
    </div>
    <div class="card-body">
        @include('admin.paket-wisata.form', compact('model'))
    </div>
</div>

@endsection
