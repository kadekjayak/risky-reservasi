@extends('layouts.admin')

@section('content')

<div class="card mb-3">
    <div class="card-header d-flex justify-content-between align-items-center">
        Laporan Reservasi Tour
    </div>
    <div class="card-body">
        {{ Form::open([
                    'route' => 'admin.reports.store', 
                    'method' => 'POST' 
                ]) }}
            <input type="hidden" name="req" value="report-reservasi-tour" />
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label>Tanggal Mulai</label>
                        <input type="date" name="start_date" class="form-control">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label>Tanggal Selesai</label>
                        <input type="date" name="end_date" class="form-control">
                    </div>
                </div>
                <div class="col-shrink">
                    <div class="form-group">
                        <label class="d-block">&nbsp;</label>
                        <button type="submit" class="btn btn-primary">Download</button>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>

<div class="card mb-3">
    <div class="card-header d-flex justify-content-between align-items-center">
        Laporan Paket Wisata
    </div>

    <div class="card-body">
        {{ Form::open([
                    'route' => 'admin.reports.store', 
                    'method' => 'POST' 
                ]) }}
            <input type="hidden" name="req" value="report-paket-tour" />
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label>Tanggal Mulai</label>
                        <input type="date" name="start_date" class="form-control">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label>Tanggal Selesai</label>
                        <input type="date" name="end_date" class="form-control">
                    </div>
                </div>
                <div class="col-shrink">
                    <div class="form-group">
                        <label class="d-block">&nbsp;</label>
                        <button type="submit" class="btn btn-primary">Download</button>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>

<div class="card mb-3">
    <div class="card-header d-flex justify-content-between align-items-center">
        Laporan Penggunaan Kendaraan
    </div>
    <div class="card-body">
    {{ Form::open([
                    'route' => 'admin.reports.store', 
                    'method' => 'POST' 
                ]) }}
            <input type="hidden" name="req" value="report-kendaraan" />
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label>Tanggal Mulai</label>
                        <input type="date" name="start_date" class="form-control">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label>Tanggal Selesai</label>
                        <input type="date" name="end_date" class="form-control">
                    </div>
                </div>
                <div class="col-shrink">
                    <div class="form-group">
                        <label class="d-block">&nbsp;</label>
                        <button type="submit" class="btn btn-primary">Download</button>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>


@endsection
