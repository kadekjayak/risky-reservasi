{{ Form::model( $model, [
                    'route' => [ 
                            ! $model->id ? 'admin.driver.store' : 'admin.driver.update', 
                            $model->id
                        ], 
                    'method' => $model->id ? 'PUT' : 'POST' 
                ]) }}
    <div class="form-group">  
        <label>Nama</label>
        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nama Driver']) }}
    </div>
    <div class="form-group">  
        <label>Nomor HP</label>
        {{ Form::text('nomor_hp', null, ['class' => 'form-control', 'placeholder' => '08xxxx']) }}
    </div>

    <div class="form-group">
        <label>Alamat</label>
        {{ Form::textarea('alamat', null, ['class' => 'form-control', 'placeholder' => 'Alamat']) }}
    </div>

    <div class="form-group">
        <label>Jenis Kelamin</label>
        {{ Form::select('jenis_kelamin', [1 => 'Laki - Laki', 2 => 'Perempuan'], 1, ['class' => 'form-control']) }}
    </div>

    <button type="submit" class="btn btn-primary">Simpan</button>
{{ Form::close() }}