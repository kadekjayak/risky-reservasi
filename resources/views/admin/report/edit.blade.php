@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        Edit Driver
    </div>
    <div class="card-body">
        @include('admin.driver.form', compact('model'))
    </div>
</div>

@endsection
