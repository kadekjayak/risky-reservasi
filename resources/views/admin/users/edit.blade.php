@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        Edit User
    </div>
    <div class="card-body">
        @include('admin.users.form', compact('model'))
    </div>
</div>

@endsection
