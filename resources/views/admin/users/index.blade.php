@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        Users
        @if( auth()->user()->role == 'administrator' )
        <a class="btn btn-primary" href="{{ route('admin.users.create') }}">Tambah Baru</a>
        @endif
    </div>

    <div class="card-body">
        <div class="row">
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nama</th>
                        <th>Email</th>
                        @if( auth()->user()->role == 'administrator' )
                        <th></th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach( $items as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->email }}</td>
                            @if( auth()->user()->role == 'administrator' )
                            <td>
                                <div class="d-flex justify-content-between">
                                    <a class="btn btn-text btn-sm" href="{{ route('admin.users.edit', $item->id) }}">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <form method="POST" action="{{ route('admin.users.destroy', $item->id) }}">
                                        <input type="hidden" name="_method" value="DELETE">
                                        
                                        <button onclick="return window.confirm('Yakin akan menghapus user ini?')" class="btn btn-text btn-sm">
                                            <i class="fa fa-trash text-danger"></i>
                                        </button>
                                    </form>
                                </div>
                            </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>

            
        </div>
    </div>
</div>

@endsection
