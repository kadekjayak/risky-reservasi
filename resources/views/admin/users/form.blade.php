{{ Form::model( $model, [
                    'route' => [ 
                            ! $model->id ? 'admin.users.store' : 'admin.users.update', 
                            $model->id
                        ], 
                    'method' => $model->id ? 'PUT' : 'POST' 
                ]) }}
    <div class="form-group">  
        <label>Nama</label>
        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nama']) }}
    </div>
    <div class="form-group">  
        <label>Email</label>
        {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) }}
    </div>
    
    <hr>

    <div class="form-group">
        <label>Password</label>
        {{ Form::password('password', ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        <label>Konfirmasi Password</label>
        {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
    </div>

    <button type="submit" class="btn btn-primary">Simpan</button>
{{ Form::close() }}