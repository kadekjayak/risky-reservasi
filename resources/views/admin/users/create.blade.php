@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        User
    </div>
    <div class="card-body">
        @include('admin.users.form')
    </div>
</div>

@endsection
