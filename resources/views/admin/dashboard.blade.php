@extends('layouts.admin')

@section('content')

<div class="card mb-4">
    <div class="card-header d-flex justify-content-between align-items-center">
        Jadwal Hari Ini
    </div>

    <div class="card-body">
        <table class="table">
            <thead>
                <tr>
                    <th>#Reservasi</th>
                    <th>Paket</th>
                    <th>Driver</th>
                    <th>Kendaraan</th>
                    
                </tr>
            </thead>
           @foreach( $today_schedule as $item )
           <tbody>
                <tr>
                    <td>
                        {{ $item->name }}<br>
                        <small>#{{ $item->kode_reservasi }}</small>
                        
                    </td>
                    <td>{{ @$item->paketTour->name }}</td>
                    <td>{{ @$item->driver->name }}</td>
                    <td>{{ @$item->jenisKendaraan->name }}</td>
                    <td>
                        {{ @$item->kendaraan->nomor_polisi }}<br>
                        <small>{{ @$item->kendaraan->jenisKendaraan->name }}</small>
                        
                    </td>
                   
                </tr>
            </tbody>
           @endforeach
        </table>
    </div>
</div>

<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        Jadwal Selanjutnya
    </div>

    <div class="card-body">
    <table class="table" valign="middle">
            <thead>
                <tr>
                    <th>Reservasi</th>
                    <th>Tanggal</th>
                    <th>Paket</th>
                    <th>Driver</th>
                    <th>Jenis Kendaraan</th>
                    
                </tr>
            </thead>
           @foreach( $next_schedule as $item )
           <tbody>
                <tr>
                    <td>
                        {{ $item->name }}<br>
                        <small>#{{ $item->kode_reservasi }}</small>
                        
                    </td>
                    <td>{{ @$item->tanggal_pickup }}</td>
                    <td>{{ @$item->paketTour->name }}</td>
                    <td>{{ @$item->driver->name }}</td>
                    <td>{{ @$item->jenisKendaraan->name }}</td>
                </tr>
            </tbody>
           @endforeach
        </table>
    </div>
</div>

@endsection
