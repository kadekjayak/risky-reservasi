@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        Jenis Kendaraan Baru
    </div>
    <div class="card-body">
        @include('admin.jenis-kendaraan.form')
    </div>
</div>

@endsection
