{{ Form::model( $model, [
                    'route' => [ 
                            ! $model->id ? 'admin.jenis-kendaraan.store' : 'admin.jenis-kendaraan.update', 
                            $model->id
                        ], 
                    'enctype' => 'multipart/form-data',
                    'method' => $model->id ? 'PUT' : 'POST' 
                ]) }}
    <div class="form-group">  
        <label>Nama Jenis kendaraan</label>
        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nama Jenis Kendaraan']) }}
    </div>

    <div class="form-group">
        <label>Harga</label>
        {{ Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Harga']) }}
    </div>

    <div class="form-group">
        <label>Jumlah Kursi</label>
        {{ Form::number('seat', null, ['class' => 'form-control', 'placeholder' => 'Jumlah Kursi']) }}
    </div>

    <div class="form-group">
        <label>Deskripsi Pendek</label>
        {{ Form::textarea('short_description', null, ['class' => 'form-control', 'placeholder' => 'Deskripsi Pendek']) }}
    </div>

    <div class="form-group">
        <label>Deskripsi</label>
        {{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Deskripsi']) }}
    </div>

    <div class="form-group">
        <label>Gambar</label>
        
        @if( $model->image )
            <div class="d-block">
                <img src="{{ Storage::url( $model->image ) }}" class="img-fluid mb-3" width="150">
            </div>
        @endif

        {{ Form::file('image') }}
    </div>

    <button type="submit" class="btn btn-primary">Simpan</button>
{{ Form::close() }}