@extends('layouts.admin')

@section('content')

<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        Kendaraan
        @if( auth()->user()->role == 'administrator')
            <a class="btn btn-primary" href="{{ route('admin.kendaraan.create') }}">Tambah Baru</a>
        @endif
    </div>

    <div class="card-body">
        <div class="row">
            @foreach( $items as $item)
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="h5 mb-0">{{ $item->jenisKendaraan->name }}</h4>
                            <strong>{{ $item->tahun }}</strong>
                            <a href="{{ url(route('admin.kendaraan.edit', $item->id)) }}">
                                <p>{{ $item->nomor_polisi }}</p>
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

@endsection
