{{ Form::model( $model, [
                    'route' => [ 
                            ! $model->id ? 'admin.kendaraan.store' : 'admin.kendaraan.update', 
                            $model->id
                        ], 
                    'method' => $model->id ? 'PUT' : 'POST' 
                ]) }}
    <div class="form-group">  
        <label>Jenis Kendaraan</label>
        {{ Form::select('jenis_kendaraan_id', $jenis_kendaraan, null, ['class'=> 'form-control'] ) }}
    </div>

    <div class="form-group">  
        <label>Nomor Polisi</label>
        {{ Form::text('nomor_polisi', null, ['class' => 'form-control', 'placeholder' => 'Nama Jenis Kendaraan']) }}
    </div>

    <div class="form-group">
        <label>Warna</label>
        {{ Form::text('warna', null, ['class' => 'form-control', 'placeholder' => 'Harga']) }}
    </div>

    <div class="form-group">
        <label>Tahun</label>
        {{ Form::number('tahun', null, ['class' => 'form-control', 'placeholder' => 'Jumlah Kursi']) }}
    </div>

    <button type="submit" class="btn btn-primary">Simpan</button>
{{ Form::close() }}