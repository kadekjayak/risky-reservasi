@extends('layouts.app')

@section('content')
@include('elements.cover-medium', ['title' => 'Register'])
<section class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">

                @include('flash::message')

                <div class="card panel-default">
                    <div class="card-header">Register</div>

                    <div class="card-body">

                        <p>Sudah punya akun? <a href="{{ url('/login') }}">Login disini</a></p>
                        <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class=" control-label">Name</label>

                                <div class="">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class=" control-label">E-Mail Address</label>

                                <div class="">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('nomor_hp') ? ' has-error' : '' }}">
                                <label class=" control-label">Nomor HP</label>

                                <div class="">
                                    <input type="text" class="form-control" name="nomor_hp" value="{{ old('nomor_hp') }}" required>

                                    @if ($errors->has('nomor_hp'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('nomor_hp') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('jenis_kelamin') ? ' has-error' : '' }}">
                                <label class=" control-label">Jenis Kelamin</label>

                                <div class="">
                                    {{ Form::select('jenis_kelamin', ['laki-laki' => 'Laki - Laki', 'perempuan' => 'Perempuan'], old('jenis_kelamin'), ['class' => 'form-control']) }}

                                    @if ($errors->has('jenis_kelamin'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('jenis_kelamin') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class=" control-label">Password</label>

                                <div class="">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class=" control-label">Confirm Password</label>

                                <div class="">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="">
                                    <button type="submit" class="btn btn-primary">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
