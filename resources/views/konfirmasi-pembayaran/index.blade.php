@extends('layouts.app')

@section('content')
    @include('elements.cover-medium', ['title' => 'Konfirmasi Pembayaran'])

    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6 offset-3">
                    @include('flash::message')

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif


                    <article>
                        <h3 class="mb-3">Konfirmasi Pembayaran</h1>

                        {{ Form::open(['route' => 'konfirmasi-pembayaran.store', 'enctype' => 'multipart/form-data', 'id' => 'FormKonfirmasiPembayaran', 'data-reservasi' => json_encode( $reservasi )]) }}

                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" value="{{ request()->user()->name }}" disabled />
                        </div>

                        <div class="form-group">
                            <label>Kode Reservasi</label>
                            {{ Form::select('reservasi_id', [null => 'Pilih Kode Reservasi'] + $reservasi->pluck('kode_reservasi', 'id')->toArray(), old('kode_reservasi') or request()->query('reservasi_id'), ['class' => 'form-control'] ) }}
                        </div>

                        <div id="detail-konfirmasi-pembayaran" style="display: none;">

                            <hr>

                            <div class="form-group">
                                <label>Nama Akun Bank</label>
                                {{ Form::text('nama_pengirim', old('nama_pengirim'), ['class' => 'form-control'] ) }}
                            </div>
                            
                            <div class="form-group">
                                <label>Tanggal Transfer</label>
                                <input type="date" name="tanggal_transfer" class="form-control" value="{{ old('tanggal_transfer') }}" />
                            </div>
                            <div class="form-group">
                                <label>Rekening Tujuan</label>
                                {{ Form::select('rekening_id', [null => 'Pilih Rekening Tujuan'] + $rekening, old('rekening_tujuan'), ['class' => 'form-control'] ) }}
                            </div>

                            <div class="form-group">
                                <label>Jenis Pembayaran</label>
                                {{ Form::select('jenis_pembayaran', [null => 'Pilih Jenis Pembayaran'] + ['full' => 'Full', 'dp' => 'DP'], old('jenis_pembayaran'), ['class' => 'form-control'] ) }}
                            </div>

                            <div class="form-group" id="field-jumlah-transfer" style="display: none;">
                                <label>Jumlah Transfer</label>
                                {{ Form::number('jumlah_transfer', old('jumlah_transfer'), ['class' => 'form-control'] ) }}
                            </div>

                            <div class="form-group">
                                <label>File</label>
                                {{ Form::file('image') }}
                            </div>

                            <button class="btn btn-primary">
                                Konfirmasi Pembayaran
                            </button>
                        </div>

                        {{ Form::close() }}
                    </article>
                </div>
            </div>
        </div>
    </section>
@endsection
