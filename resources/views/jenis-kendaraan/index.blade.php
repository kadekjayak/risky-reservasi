@extends('layouts.app')

@section('content')

@include('elements.cover-medium', ['title' => 'Kendaraan', 'subtitle' => 'Pilih kendaraan untuk pergi ke manapun yang anda mau.'])

<section class="py-5">
    <div class="container">
        <div class="row">
            @foreach( $items as $item)
                <div class="col-md-3">
                    <div class="card">
                        <a href="{{ route('jenis-kendaraan.show', $item->id)}}">
                            <img class="card-img-top" src="{{ Storage::url( $item->image) }}" alt="Card image cap">
                        </a>
                        <div class="card-body">
                            <h4 class="h5 mb-0">{{ $item->name }}</h4>

                            <p class="mb-0">{{ $item->short_description }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

</section>

@endsection
