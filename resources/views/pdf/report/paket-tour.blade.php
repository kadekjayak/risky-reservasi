<style type="text/css">
    .table {
        width: 100%;
        border-collapse: collapse;
    }
</style>
<div id="header" style="margin-bottom: 1cm;">
    <h1>Laporan Paket Tour</h1>
    {{ $start_date }} - {{ $end_date }}
</div>

<table cellpadding="1" class="table" border="1">
    <thead>
        <tr>
            <th>No.</th>
            <th>Paket</th>
            <th>Jumlah Reservasi</th>
            <th>Total Pemasukan</th>
        </tr>
    </thead>
    <tbody>
        @foreach( $paket_wisata as $index => $item )
            <tr>
                <td>
                    {{ $index + 1 }}.
                </td>
                <td>
                    {{ $item->name }}
                </td>
                <td>
                    {{ $item->jumlah_reservasi }}
                </td>
                <td>
                    {{ $item->total_pembayaran }}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>