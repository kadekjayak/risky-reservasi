<style type="text/css">
    .table {
        width: 100%;
        border-collapse: collapse;
    }
</style>
<div id="header" style="margin-bottom: 1cm;">
    <h1>Laporan Reservasi</h1>
    <p>Periode: {{ $start_date }} - {{ $end_date }}</p>
</div>

<table cellpadding="1" class="table" border="1">
    <thead>
        <tr>
            <th>No.</th>
            <th>Paket</th>
            <th>Customer</th>
            <th>Penumpang</th>
            <th>Tanggal Tour</th>
            <th>Kendaraan</th>
            <th>Driver</th>
            <th>Total Harga</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        @foreach( $reservasi as $index => $item )
            <tr>
                <td>
                    {{ $index + 1 }}.
                </td>
                <td>
                    {{ $item->paketTour->name }}
                </td>
                <td>
                    {{ $item->user->name }}
                </td>
                <td>
                    {{ $item->jumlah_penumpang }}
                </td>
                <td>
                    {{ $item->tanggal_pickup }}
                </td>
                <td>
                    {{ $item->jenisKendaraan->name }} - {{ $item->kendaraan->nomor_polisi }}
                </td>
                <td>
                    {{ $item->driver->name }}
                </td>
                <td>
                    {{ $item->getFormatedTotal() }}
                </td>
                <td>
                    {{ $item->statusReservasi->name }}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>