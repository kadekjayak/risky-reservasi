<style type="text/css">
    .table {
        width: 100%;
        border-collapse: collapse;
    }
</style>
<div id="header" style="margin-bottom: 1cm;">
    <h1>Laporan Kendaraan</h1>
    {{ $start_date }} - {{ $end_date }}
</div>

<table cellpadding="1" class="table" border="1">
    <thead>
        <tr>
            <th>No.</th>
            <th>Jenis Kendaraan</th>
            <th>Warna</th>
            <th>Nomor Polisi</th>
            <th>Jumlah Reservasi</th>
        </tr>
    </thead>
    <tbody>
        @foreach( $kendaraan as $index => $item )
            <tr>
                <td>
                    {{ $index + 1 }}.
                </td>
                <td>
                    {{ $item->jenisKendaraan->name }}
                </td>
                <td>
                    {{ $item->warna }}
                </td>
                <td>
                    {{ $item->nomor_polisi }}
                </td>
                <td>
                    {{ $item->jumlah_reservasi }}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>