@extends('layouts.user')

@section('content')
    <div class="container">
        
        <h3>Reservasi</h3>
            @foreach( $items as $item)
                
                <div class="card mb-3">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <span>
                                <a href="{{ route('reservasi.show', $item->kode_reservasi) }}">
                                    #{{ $item->kode_reservasi }}
                                </a>
                            </span>
                            <div class="badge badge-warning">
                                {{ $item->statusReservasi->name }}
                            </div>
                        </div>
                        <small>{{ $item->tanggal_pickup }}</small>
                    </div>
                    <div class="card-body">
                        <div class="paket-tour">
                            @if( $item->paketTour )
                                <div class="media mb-3">
                                    <img width="100" class="mr-2 rounded" src="{{ $item->paketTour->getImageUrl() }}" />
                                    <div class="media-body">
                                        <strong>{{ $item->paketTour->name }}</strong><br>
                                        <small>Rp. {{ Helper::formatMoney( $item->harga_paket_tour ) }}</small>
                                    </div>
                                </div>
                            @endif

                            @if( $item->jenisKendaraan )
                                <div class="media mb-3">
                                    <img width="100" class="mr-2 rounded" src="{{ $item->jenisKendaraan->getImageUrl() }}" />
                                    <div class="media-body">
                                        <strong>{{ $item->jenisKendaraan->name }}</strong><br>
                                        <small>Rp. {{ Helper::formatMoney( $item->harga_jenis_kendaraan ) }}</small>
                                    </div>
                                </div>
                            @endif

                            <hr>
                            <div class="d-flex justify-content-between align-items-center">
                                <strong>Rp. {{ Helper::formatMoney( $item->total ) }}</strong>

                                @if( $item->status_reservasi_id == 1)
                                    <a href="{{ route('konfirmasi-pembayaran.index',  ['reservasi_id' => $item->id]) }}" class="btn btn-sm btn-primary">Konfirmasi Pembayaran</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            
            @endforeach
        </div>
    </div>
@endsection
