@extends('layouts.app')

@section('content')
<section id="cover" class="overlay-dark overlay-darken py-5 justify-content-center" style="background-size: cover; background-image: url('{{ $model->getImageUrl() ?: url('/img/cover-tour.jpg') }}')">
    <div class="container text-center text-white" style="position: relative; z-index: 1;">
        <div class="pt-5">
            <h1 class="text-white">{{ $model->name }}</h1>
            <div class="divider short p-2"></div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb pl-0 pt-0 justify-content-center bg-transparent text-light">
                    <li class="breadcrumb-item">
                        <a href="{{ url('/') }}">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('paket-tour.index') }}">Paket Tour</a>
                    </li>
                    <li class="active breadcrumb-item ">
                        {{ $model->name }}
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</section>

<section class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <article>

                    <figure>
                        <img src="{{ $model->getImageUrl() }}" class="img-fluid rounded">
                    </figure>
                    
                    <p>{!! $model->description !!}</p>
                </article>
            </div>
            <div class="col-md-4">
                <div class="card bg-primary text-light mb-3">
                    <div class="card-body text-center">
                        <sup>Start From<sup>
                        <div class="h2 mb-0 text-white">{{ $model->getFirstFormatedPrice() }}</div>
                    </div>
                </div>

                @if( request()->user() ) 
                    <button class="btn btn-primary btn-block btn-lg" id="BookingTour">
                        Booking Tour Ini
                    </button>
                @else
                    <a href="{{ url('/register') }}" class="btn btn-primary btn-block btn-lg" >
                        Booking Tour Ini
                    </a>
                @endif

                <div class="modal fade" tabindex="-1" role="dialog" id="Modal-Booking">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Reservasi: {{ $model->name }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <input type="hidden" name="paket_tour_id" value="{{ $model->id }}"/>
                                    <input type="hidden" name="paket_tour" value="{{ $model->name }}"/>
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input type="text" name="name" class="form-control" value="{{ request()->user() ? request()->user()->name : '' }}" />
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="email" class="form-control" value="{{ request()->user() ? request()->user()->email : '' }}" />
                                    </div>
                                    <div class="form-group">
                                        <label>Nomor HP</label>
                                        <input type="email" name="nomor_hp" class="form-control" value="{{ request()->user() ? request()->user()->nomor_hp : '' }}" />
                                    </div>
                                    <div class="form-group">
                                        <label class="label-penumpang">Jumlah Penumpang</label>
                                        @if( $model->hargaPaketTour )
                                            <select class="form-control" name="jumlah_penumpang" value="1">
                                                @foreach( $model->hargaPaketTour as $harga )
                                                    <option value="{{ $harga->jumlah_penumpang }}">{{ $harga->jumlah_penumpang }} orang - ( {{ $harga->getFormatedPrice() }} )</option>
                                                @endforeach
                                            </select>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>Tanggal Tour</label>
                                        <input type="text" name="tanggal_pickup" value="{{ \Carbon\Carbon::now()->addDay(3)->format('Y-m-d') }}" class="form-control datepicker" />
                                    </div>
                                    <div class="form-group alamat-pickup">
                                        <label>Alamat Pickup</label>
                                        <textarea type="email" name="alamat_pickup" class="form-control"></textarea>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-text" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary submit" data-toggle="modal">Selanjutnya</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" tabindex="-1" role="dialog" id="Modal-Booking-Mobil">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Pilih Mobil</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="list-kendaraan">
                                    <div class="d-flex justify-content-center">
                                        <div class="spinner-border" role="status">
                                            <span class="sr-only">Loading...</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-text back">Kembali</button>
                                <button type="button" class="btn btn-primary submit">Selanjutnya</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" tabindex="-1" role="dialog" id="Modal-Booking-Konfirmasi">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Konfirmasi Booking</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="detail-booking">
                                    <div class="d-flex justify-content-center">
                                        <div class="spinner-border" role="status">
                                            <span class="sr-only">Loading...</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-text back">Kembali</button>
                                <button type="button" class="btn btn-primary submit">Booking</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@endsection
